$solution = $args[0]
$artifactHome = $args[1]
$releaseEnv = $args[2]



function deployISPAC 
(
	[string]$ispacFile	
    ,[string]$solution	
    ,[string]$projectName	
    ,[string]$server
)
{
    $prefix = ""
    if ($releaseEnv -ne "Production")
    {
        $prefix = "$releaseEnv" + "_"
    }

    $projectFolder = $prefix + "$solution"
    $ssisServer = $server
    $ssisdb = "SSISDB"
    $sqlConnectionString = "Data Source=$ssisServer;Initial Catalog=ssisdb;Integrated Security=SSPI;"

    $SSISNamespace = "Microsoft.SqlServer.Management.IntegrationServices";
    $loadStatus = [System.Reflection.Assembly]::LoadWithPartialName($SSISNamespace) | Out-Null;    
    $sqlConnection = New-Object System.Data.SqlClient.SqlConnection $sqlConnectionString
    $integrationServices = New-Object $SSISNamespace".IntegrationServices" $sqlConnection   
        
    # Create folder in SSISDB if doesn't exist      
    write-host "Will create $projectFolder if doesn't exist in $ssisdb."
    $initCMD = "if not exists (SELECT [name] FROM [internal].[folders] where name = '$projectFolder')
                begin
	                exec [catalog].[create_folder] @folder_name = '$projectFolder'; 
                end
                
                if exists 
                (
	                select	1 
	                from	[internal].[environments] e
			                inner join [internal].[folders] f on e.folder_id = f.folder_id
	                where	[environment_name] = '$projectFolder'
	                and		f.[name] = '$projectFolder'
                )
                begin
	                declare @ref int = -1; 

	                select	@ref = reference_id
	                from	[internal].[environment_references] er
			                inner join [internal].[projects] p on p.project_id = er.project_id
			                inner join [internal].[folders] f on p.folder_id = f.folder_id
	                where	er.reference_type = 'R'
	                and		environment_name = '$projectFolder'
	                and		p.name = '$projectName'
	                and		f.[name] = '$projectFolder';

	                if @ref > -1
	                begin
		                exec [catalog].[delete_environment_reference] @reference_id = @ref; 
	                end

	                exec [catalog].[delete_environment] @folder_name = '$projectFolder', @environment_name = '$projectFolder';
                end		

                if exists 
                (
	                select	1 
	                from	[internal].[projects] p
			                inner join [internal].[folders] f on p.folder_id = f.folder_id
	                where	p.name = '$projectName'
	                and		f.[name] = '$projectFolder'
                )
                begin
	                exec [catalog].[delete_project] @folder_name = '$projectFolder', @project_name = '$projectName';
                end"
    
    Invoke-Sqlcmd -ServerInstance $ssisServer -Query $initCMD -Database $ssisdb


    # Deploy.    
    [byte[]] $rawFile = [System.IO.File]::ReadAllBytes($ispacFile)
    $catalog = $integrationServices.Catalogs[$ssisdb] 
    $folder = $catalog.Folders[$projectFolder]
    $folder.DeployProject($projectName, $rawFile)     
       

    write-host "Creating environment $projectFolder and referencing to project $projectName"
    $postCMD = "declare @ref int; 
                exec [catalog].[create_environment] @folder_name = '$projectFolder', @environment_name = '$projectFolder';
                exec [catalog].[create_environment_reference] @environment_name = '$projectFolder', @project_name = '$projectName', @folder_name = '$projectFolder', @reference_type=R, @reference_id = @ref OUTPUT;
                "

    Invoke-Sqlcmd -ServerInstance $ssisServer -Query $postCMD -Database $ssisdb
    

    # Create environment variable if ISCatalogEnvironment.xml is found in Build folder. 
    # First create environment variables. 
    # Then map variables to properties in an SSIS project
    # Inside a project, properties can be at project, configuration manager and package levels. 
    $catalogXML = "$artifactHome\Artifact\SSIS\$($solution)ISCatalogEnvironment.xml"

    if (Test-Path $catalogXML)
    {
        write-host "Found ISCatalogEnvironment.xml. Will deploy."
        [xml]$XmlDocument = Get-Content -Path $catalogXML

        write-host "Creating environment variables."
        foreach ($v in $XmlDocument.Environment.Variables.Variable)
        {    
            $value = ""
            $valueTemp = ""
            $dateInit = ""

            if ($releaseEnv -eq "Integration")
            {
                $valueTemp = $v.ValueIntegration
            }
            elseif ($releaseEnv -eq "Testing")
            {
                $valueTemp = $v.ValueTesting
            }
            elseif ($releaseEnv -eq "Production")
            {
                $valueTemp = $v.ValueProduction
            }

            if ($v.Type -match "Int" -or $v.Type -eq "Double" -Or $v.Type -eq "Single" -or $v.Type -eq "Boolean")
            {
                $value = "$valueTemp"
            } 
            elseif ($v.Type -eq "DateTime")  
            {
                $value = "@var"
                $dateInit = "declare @var datetime = N'$valueTemp'"
            }
            else
            {
                $value = "N'$valueTemp'"
            }
                 
            $varCMD = "$dateInit
                       exec [catalog].[create_environment_variable] @variable_name = N'$($v.Name)'
                                                                    ,@sensitive = $($v.IsSensitive)
                                                                    ,@description = N''
                                                                    ,@environment_name = N'$($projectFolder)'
                                                                    ,@folder_name = N'$($projectFolder)'
                                                                    ,@value = $value
                                                                    ,@data_type = N'$($v.Type)';
                        "
            
            Invoke-Sqlcmd -ServerInstance $ssisServer -Query $varCMD -Database $ssisdb    
        }

        foreach ($p in $XmlDocument.Environment.Projects.Project)
        {            
            write-host "Mapping connection manager variables in $($p.Name) project."
            foreach ($cm in $p.ConnectionManagers.ConnectionManager)
            {
                $cmCMD = "exec [catalog].[set_object_parameter_value] @object_type = 20
                                                                    ,@parameter_name = N'CM.$($cm.Name).$($cm.Property)'
                                                                    ,@object_name = N'$($projectName)'
                                                                    ,@folder_name = N'$($projectFolder)'
                                                                    ,@project_name = N'$($projectName)'
                                                                    ,@value_type = R
                                                                    ,@parameter_value = N'$($cm.EnvironmentVariable)';
                        "
                
                Invoke-Sqlcmd -ServerInstance $ssisServer -Query $cmCMD -Database $ssisdb
            }

            write-host "Mapping project variables in $($p.Name) project."
            foreach ($pp in $p.ProjectParameters.ProjectParameter)
            {
                $ppCMD = "exec [catalog].[set_object_parameter_value] @object_type = 20
                                                                    ,@parameter_name = N'$($pp.Name)'
                                                                    ,@object_name = N'$($projectName)'
                                                                    ,@folder_name = N'$($projectFolder)'
                                                                    ,@project_name = N'$($projectName)'
                                                                    ,@value_type = R
                                                                    ,@parameter_value = N'$($pp.EnvironmentVariable)';
                        "
                
                Invoke-Sqlcmd -ServerInstance $ssisServer -Query $ppCMD -Database $ssisdb
            }

            write-host "Mapping package variables in $($p.Name) project."
            foreach ($pk in $p.PackageParameters.Package)
            {
                foreach ($prm in $pk.Parameters.PackageParameter)
                {
                    $pkCMD = "exec [catalog].[set_object_parameter_value] @object_type = 30
                                                                        ,@parameter_name = N'$($prm.Name)'
                                                                        ,@object_name = N'$($pk.Name)'
                                                                        ,@folder_name = N'$($projectFolder)'
                                                                        ,@project_name = N'$($projectName)'
                                                                        ,@value_type = R
                                                                        ,@parameter_value = N'$($prm.EnvironmentVariable)';
                            "
                
                    Invoke-Sqlcmd -ServerInstance $ssisServer -Query $pkCMD -Database $ssisdb
                }                  
            }
        }
    }
}



function New-SSRSWebServiceProxy
{
	param (
		[parameter(Mandatory=$true)]
		[ValidatePattern('^https?://')]
		[string]$Uri
		,[System.Management.Automation.PSCredential]$Credential
	)

	$script:ErrorActionPreference = 'Stop'
	Set-StrictMode -Version Latest

	$Assembly = [AppDomain]::CurrentDomain.GetAssemblies() | Where-Object {$_.GetType('SSRS.ReportingService2010.ReportingService2010')}

	if (($Assembly | Measure-Object).Count -gt 1) 
    {
		throw 'AppDomain contains multiple definitions of the same type. Restart PowerShell host.'
	}

	if (-not $Assembly) 
    {
		if ($Credential) 
        {
			$CredParams = @{ Credential = $Credential }
		} else {
			$CredParams = @{ UseDefaultCredential = $true }
		}
        
		$Proxy = New-WebServiceProxy -Uri $Uri -Namespace SSRS.ReportingService2010 @CredParams
	} 
    else 
    {
		$Proxy = New-Object -TypeName SSRS.ReportingService2010.ReportingService2010
		if ($Credential) 
        {
			$Proxy.Credentials = $Credential.GetNetworkCredential()
		} 
        else 
        {
			$Proxy.UseDefaultCredentials = $true
		}
	}

	$Proxy.Url = $Uri
	return $Proxy
}



function deploySSRS 
(
    [string]$solution	
    ,[string]$projectFolder	
    ,[string]$server
)
{
    $prefix = "/"
    if ($releaseEnv -ne "Production")
    {
        $prefix = "/$releaseEnv"
    }
    
    $ssrs = "$server/ReportService2010.asmx?wsdl"
    $proxy = New-SSRSWebServiceProxy -Uri $ssrs

    Get-ChildItem $projectFolder -Filter *.rptproj | Foreach-Object {        
        $projectFile = $_.FullName
        $projectName = $_.Name -replace ".rptproj", ""
      
        write-host "Found $projectFile. Deploying"   
         
        # Clean up existing folder to ensure no residual issues when testing what is newly deployed.   
        foreach ($f in $proxy.ListChildren("/", $false))
        {
            if ($f.Name -eq ($prefix -replace "/", "") -and $f.TypeName -eq "Folder")
            {
                foreach ($f in $proxy.ListChildren($prefix, $false))
                {
                    if ($f.Name -eq $solution -and $f.TypeName -eq "Folder")
                    {
                        $Proxy.DeleteItem("$prefix/$solution")
                    }
                } 
            }
        }
        
        # Open up rptproj and make sure BUILD configuration has correct values. 
        [xml]$XmlDocument = Get-Content -Path $projectFile

        foreach ($pg in $XmlDocument.Project.PropertyGroup)
        {
            if ($pg.Condition -match "BUILD")
            {
                $pg.TargetServerURL = $server
                $pg.FullPath = "BUILD"
                $pg.OutputPath = "bin\BUILD"
                $pg.TargetReportFolder = "$releaseEnv/$solution/$projectName/Report"
                $pg.TargetDatasetFolder = "$releaseEnv/$solution/$projectName/DataSet"
                $pg.TargetDatasourceFolder = "$releaseEnv/$solution/$projectName/DataSource"
                $pg.TargetReportPartFolder = "$releaseEnv/$solution/$projectName/ReportParts"
                $pg.TargetServerVersion = "SSRS2016"
                $pg.OverwriteDatasets = "True"
                $pg.OverwriteDataSources = "True"                
            }
        }
        
        $XmlDocument.Save($projectFile)

        # Deploy project. Details of where are recorded in project's configuration named BUILD. 
        $devEnv = "${env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\SQL\Common7\IDE\devenv.com"
        & "$devEnv" "$projectFile" /Deploy BUILD
    }
}



function deployDACPAC 
(
	[string]$dacpacFile	
    ,[string]$solution	
    ,[string]$dbName	
    ,[string]$artifactFolder
    ,[string]$server
)
{
    $sqlpkg = "${env:ProgramFiles}\Microsoft SQL Server\150\DAC\bin\SqlPackage.exe"

    $xmlFile = "$artifactFolder\$dbName.publish.xml"
    $outputFile = "$artifactFolder\$dbName.sql"
    
    # Set a default profile for BUILD and generate SQL script. Echo the script out to console. Can't publish artifacts in Release pipeline. 
    # This portion will eventually become an actual publish to physical database (/Action:Script to /Action:Publish)
    $xml = "<?xml version=`"1.0`" encoding=`"utf-8`"?>
            <Project ToolsVersion=`"15.0`" xmlns=`"http://schemas.microsoft.com/developer/msbuild/2003`">
              <PropertyGroup>
                <IncludeCompositeObjects>True</IncludeCompositeObjects>
                <TargetDatabaseName>Audb_IAL_HDS_BI</TargetDatabaseName>
                <DeployScriptFileName>Audb_IAL_HDS_BI.sql</DeployScriptFileName>
                <TargetConnectionString>Data Source=AZ1STGPBI01INST01;Integrated Security=True;Persist Security Info=False;Pooling=False;MultipleActiveResultSets=False;Connect Timeout=60;Encrypt=False;TrustServerCertificate=True</TargetConnectionString>
                <ProfileVersionNumber>1</ProfileVersionNumber>
                <BlockOnPossibleDataLoss>True</BlockOnPossibleDataLoss>
                <ScriptDatabaseOptions>False</ScriptDatabaseOptions>
                <DropIndexesNotInSource>False</DropIndexesNotInSource>
                <DropExtendedPropertiesNotInSource>False</DropExtendedPropertiesNotInSource>
                <DropDmlTriggersNotInSource>False</DropDmlTriggersNotInSource>
                <DropConstraintsNotInSource>False</DropConstraintsNotInSource>
                <ScriptRefreshModule>False</ScriptRefreshModule>
                <DropStatisticsNotInSource>False</DropStatisticsNotInSource>
              </PropertyGroup>
            </Project>" -replace "Audb_IAL_HDS_BI", $dbName -replace "AZ1STGPBI01INST01", $server

    $xml | out-File -FilePath $xmlFile -Encoding utf8
    & "$sqlpkg" /Action:Script /SourceFile:$dacpacFile /Profile:$xmlFile /OutputPath:$outputFile

    Write-host "##vso[task.uploadfile]$outputFile"
}


$server = ""
$ssrsServer = ""
if($releaseEnv -eq "Integration")
{
    $server = "AZ1-STGPBI01\INST01"
    $ssrsServer = "http://az1-stgpbi01/ReportServer"
}
elseif($releaseEnv -eq "Testing")
{
    $server = "AZ1-STGPBI01\INST01"
    $ssrsServer = "http://az1-stgpbi01/ReportServer"
}
elseif($releaseEnv -eq "Production")
{
    $server = "AZ1-PRDPBI01\INST01"
    $ssrsServer = "http://az1-prdpbi01/ReportServer"
}


# Deploy any ISPAC found in SSIS folder
write-host "Looking for ISPAC files"
$artifactFolder = "$artifactHome\Artifact\SSIS"

if(test-path $artifactFolder)
{
    Get-ChildItem $artifactFolder -Filter *.ispac | Foreach-Object {        
        $ispacFile = $_.FullName
        $projectName = $_.Name -replace ".ispac", ""
      
        write-host "Found $ispacFile. Deploying"      
        deployISPAC -ispacFile $ispacFile -solution $solution -projectName $projectName -server $server
    }
}


# Deploy any SSRS project found in SSRS folder
write-host "Looking for SSRS projects"
$artifactFolder = "$artifactHome\Artifact\SSRS"

if(test-path $artifactFolder)
{
    Get-ChildItem $artifactFolder | Where-Object { $_.PSIsContainer -eq $true } | Foreach-Object {        
        $projectFolder = $_.FullName
      
        write-host "Found $projectFolder. Looking for rptproj"      
        deploySSRS -solution $solution -projectFolder $projectFolder -server $ssrsServer
    }
}


# Deploy any DACPAC found in SQL folder
write-host "Looking for DACPAC projects"
$artifactFolder = "$artifactHome\Artifact\SQL"

if(test-path $artifactFolder)
{
    Get-ChildItem $artifactFolder -Filter *.dacpac | Foreach-Object {            
        $dacpacFile = $_.FullName
        $dbName = $_.Name -replace ".dacpac", ""
      
        write-host "Found $dacpacFile. Deploying"      
        deployDACPAC -dacpacFile $dacpacFile -solution $solution -dbName $dbName -artifactFolder $artifactFolder -server $server
    }
}
