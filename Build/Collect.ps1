$solution = $args[0]
$artifactHome = $args[1]
$solutionPath = "BUILD/Build_Solution/$solution.sln"
$buildConfiguration = "BUILD"



function collectDeploy
(
    [string]$solution
)
{
    # Look for powershell scripts and copy over to artifact directory. Should only be 1. 
    $artifactFolder = "$artifactHome\DEPLOY"    
    $psPath = "Build\"
    
    Get-ChildItem "$psPath" -Filter *.ps1 | Foreach-Object {        
        $psFile = $_.FullName               
        
        write-host "Found $psFile and will copy to $artifactFolder"
        Copy-Item -Path $psFile -Destination $artifactFolder -Force 
    } 


    # Look for ISCatalog xml file and copy over to artifact directory. Should only be 1. 
    $artifactFolder = "$artifactHome\SSIS"    
    $xmlPath = "Build\Build_Solution\"
    $xmlFilePattern = "$solution" + "*.xml"
    
    Get-ChildItem "$xmlPath" -Filter $xmlFilePattern | Foreach-Object {        
        $xmlFile = $_.FullName               
        
        write-host "Found $xmlFile and will copy to $artifactFolder"
        Copy-Item -Path $xmlFile -Destination $artifactFolder -Force 
    } 
}



function collectDACPAC 
(
	[string]$projectFile	
    ,[string]$projectName	
    ,[string]$solution	
    ,[string]$buildConfiguration	
)
{
    # Look for dacpac and copy over to artifact directory. Should only be 1. 
    $artifactFolder = "$artifactHome\SQL"    
    $dacpacPath = "$projectFile\bin\$buildConfiguration".Substring(6) -replace "$projectName.sqlproj", ""
    
    Get-ChildItem "$dacpacPath" -Filter *.dacpac | Foreach-Object {        
        $dacpacFile = $_.FullName               
        
        write-host "Found $dacpacFile and will copy to $artifactFolder"
        Copy-Item -Path $dacpacFile -Destination $artifactFolder -Force 
    } 
}



function collectISPAC 
(
	[string]$projectFile	
    ,[string]$projectName	
    ,[string]$solution	
    ,[string]$buildConfiguration	
)
{
    # Look for ispac and copy over to artifact directory. Should only be 1. 
    $artifactFolder = "$artifactHome\SSIS"    
    $ispacPath = "$projectFile\bin\$buildConfiguration".Substring(6) -replace "$projectName.dtproj", ""
    
    Get-ChildItem "$ispacPath" -Filter *.ispac | Foreach-Object {        
        $ispacFile = $_.FullName               
        
        write-host "Found $ispacFile and will copy to $artifactFolder"
        Copy-Item -Path $ispacFile -Destination $artifactFolder -Force 
    } 
}



function collectSSRS 
(
	[string]$projectName
    ,[string]$buildConfiguration	
    ,[string]$solutionPath
    ,[string]$projectFile
    ,[string]$solution
)
{
    # Look for project folder and copy entire folder (minus sub-folders) over to artifact directory.Should be only 1 project folder.
    # Create the project folder under SSRS beforehand.   
    $artifactFolder = "$artifactHome\SSRS\$projectName"  
    $ssrsPath = "$projectFile".Substring(6) -replace "$projectName.rptproj", ""
       
    if(!(test-path $artifactFolder))
    {
          New-Item -ItemType Directory -Force -Path $artifactFolder
    }   
    
    write-host "Found $ssrsPath folder and will copy to $artifactFolder"
    Get-ChildItem $ssrsPath | Where-Object { $_.PSIsContainer -eq $false } | Foreach-Object { 
        Copy-Item -Path $_.FullName -Destination $artifactFolder -Force 
    }    
}



# Set the working folder so that relative paths work consistently across build and development environments 
[Environment]::CurrentDirectory = (Split-Path $MyInvocation.MyCommand.Path -Parent ) -replace "\\Build", ""
[Environment]::CurrentDirectory

# Look through sln file and call the appropriate action depending on the project type
write-host "Setting up artifact folders in $artifactHome"
$artifactFolders = "SSIS", "SSRS", "SQL", "DEPLOY"

foreach ($f in $artifactFolders)
{
    $path = "$artifactHome\$f"
    if(!(test-path $path))
    {
          New-Item -ItemType Directory -Force -Path $path
    }
}

write-host "Reading $solutionPath"
Get-Content "$solutionPath" | Select-String 'Project\(' | ForEach-Object {      
    $projectParts = $_ -Split '[,=]' | ForEach-Object { $_.Trim('[ "{}]') };
      
    $projectName = $projectParts[1];
    $projectFile = $projectParts[2];
    $projectGuid = $projectParts[3];

    collectDeploy -solution $solution
      
    write-host "Found $projectName at $projectFile"      
    if ($projectFile -match ".dtproj") 
    {                
        Write-Host "This is a dtproj file"
        collectISPAC -projectFile $projectFile -solution $solution -projectName $projectName -buildConfiguration $buildConfiguration
    }
    elseif ($projectFile -match ".rptproj") 
    {                
        Write-Host "This is a rptproj file"
        collectSSRS -solutionPath $solutionPath -buildConfiguration $buildConfiguration -projectName $projectName -projectFile $projectFile -solution $solution
    }
    elseif ($projectFile -match ".sqlproj") 
    {                
        Write-Host "This is a sqlproj file"
        collectDACPAC -solutionPath $solutionPath -buildConfiguration $buildConfiguration -projectName $projectName -projectFile $projectFile -solution $solution
    }
    else 
    {           
        Write-Host "Unknown project, will not be worked on"        
    }
}
