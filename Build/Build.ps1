$solution = $args[0]
$solutionPath = "Build/Build_Solution/$solution.sln"
$buildConfiguration = "BUILD"
$devEnv = "${env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\Enterprise\Common7\IDE\devenv.com"

write-host "Building $solutionPath"


# cmd /c "$devEnv" "$solutionPath" /Build $buildConfiguration

& "$devEnv" "$solutionPath" /Build $buildConfiguration
