$solution = $args[0]
$solutionPath = "BUILD/$solution/$solution.sln"
$buildConfiguration = "BUILD"

function configEnvironment
(
	[string]$projectFolder	
    ,[string]$projectName	
    ,[string]$solution	
    ,[string]$buildConfiguration	
)
{
    $catalogXML = "BUILD/$solution/ISCatalogEnvironment.xml"

    # First create environment variables. 
    # Then map variables to properties in an SSIS project
    # Inside a project, properties can be at project, configuration manager and package levels. 
    if (Test-Path $catalogXML)
    {
        write-host "Found ISCatalogEnvironment.xml. Will deploy."
        [xml]$XmlDocument = Get-Content -Path $catalogXML

        write-host "Creating environment variables."
        foreach ($v in $XmlDocument.Environment.Variables.Variable)
        {    
            $value = ""
            $dateInit = ""

            if ($v.Type -match "Int" -or $v.Type -eq "Double" -Or $v.Type -eq "Single" -or $v.Type -eq "Boolean")
            {
                $value = "$($v.Value)"
            } 
            elseif ($v.Type -eq "DateTime")  
            {
                $value = "@var"
                $dateInit = "declare @var datetime = N'$($v.Value)'"
            }
            else
            {
                $value = "N'$($v.Value)'"
            }
                 
            $varCMD = "$dateInit
                       exec [catalog].[create_environment_variable] @variable_name = N'$($v.Name)'
                                                                    ,@sensitive = $($v.IsSensitive)
                                                                    ,@description = N''
                                                                    ,@environment_name = N'$($projectFolder)'
                                                                    ,@folder_name = N'$($projectFolder)'
                                                                    ,@value = $value
                                                                    ,@data_type = N'$($v.Type)';
                        "
            
            Invoke-Sqlcmd -ServerInstance $ssisServer -Query $varCMD -Database $ssisdb    
        }

        foreach ($p in $XmlDocument.Environment.Projects.Project)
        {            
            write-host "Mapping connection manager variables in $($p.Name) project."
            foreach ($cm in $p.ConnectionManagers.ConnectionManager)
            {
                $cmCMD = "exec [catalog].[set_object_parameter_value] @object_type = 20
                                                                    ,@parameter_name = N'CM.$($cm.Name).$($cm.Property)'
                                                                    ,@object_name = N'$($projectName)'
                                                                    ,@folder_name = N'$($projectFolder)'
                                                                    ,@project_name = N'$($projectName)'
                                                                    ,@value_type = R
                                                                    ,@parameter_value = N'$($cm.EnvironmentVariable)';
                        "
                
                Invoke-Sqlcmd -ServerInstance $ssisServer -Query $cmCMD -Database $ssisdb
            }

            write-host "Mapping project variables in $($p.Name) project."
            foreach ($pp in $p.ProjectParameters.ProjectParameter)
            {
                $ppCMD = "exec [catalog].[set_object_parameter_value] @object_type = 20
                                                                    ,@parameter_name = N'$($pp.Name)'
                                                                    ,@object_name = N'$($projectName)'
                                                                    ,@folder_name = N'$($projectFolder)'
                                                                    ,@project_name = N'$($projectName)'
                                                                    ,@value_type = R
                                                                    ,@parameter_value = N'$($pp.EnvironmentVariable)';
                        "
                
                Invoke-Sqlcmd -ServerInstance $ssisServer -Query $ppCMD -Database $ssisdb
            }

            write-host "Mapping package variables in $($p.Name) project."
            foreach ($pk in $p.PackageParameters.Package)
            {
                foreach ($prm in $pk.Parameters.PackageParameter)
                {
                    $pkCMD = "exec [catalog].[set_object_parameter_value] @object_type = 30
                                                                        ,@parameter_name = N'$($prm.Name)'
                                                                        ,@object_name = N'$($pk.Name)'
                                                                        ,@folder_name = N'$($projectFolder)'
                                                                        ,@project_name = N'$($projectName)'
                                                                        ,@value_type = R
                                                                        ,@parameter_value = N'$($prm.EnvironmentVariable)';
                            "
                
                    Invoke-Sqlcmd -ServerInstance $ssisServer -Query $pkCMD -Database $ssisdb
                }                  
            }
        }
    }
}

function deployISPAC 
(
	[string]$projectFile	
    ,[string]$projectName	
    ,[string]$solution	
    ,[string]$buildConfiguration	
)
{
    $projectFolder = "$buildConfiguration" + "_" + "$solution"
    $ssisServer = "AZ1-STGPBI01\INST01"
    $ssisdb = "SSISDB"
    $sqlConnectionString = "Data Source=$ssisServer;Initial Catalog=ssisdb;Integrated Security=SSPI;"

    $SSISNamespace = "Microsoft.SqlServer.Management.IntegrationServices";
    $loadStatus = [System.Reflection.Assembly]::LoadWithPartialName($SSISNamespace) | Out-Null;    
    $sqlConnection = New-Object System.Data.SqlClient.SqlConnection $sqlConnectionString
    $integrationServices = New-Object $SSISNamespace".IntegrationServices" $sqlConnection   
        
    # Create folder in SSISDB if doesn't exist      
    write-host "Will create $projectFolder if doesn't exist in $ssisdb."
    $initCMD = "if not exists (SELECT [name] FROM [internal].[folders] where name = '$projectFolder')
                begin
	                exec [catalog].[create_folder] @folder_name = '$projectFolder'; 
                end
                
                if exists 
                (
	                select	1 
	                from	[internal].[environments] e
			                inner join [internal].[folders] f on e.folder_id = f.folder_id
	                where	[environment_name] = '$projectFolder'
	                and		f.[name] = '$projectFolder'
                )
                begin
	                declare @ref int = -1; 

	                select	@ref = reference_id
	                from	[internal].[environment_references] er
			                inner join [internal].[projects] p on p.project_id = er.project_id
			                inner join [internal].[folders] f on p.folder_id = f.folder_id
	                where	er.reference_type = 'R'
	                and		environment_name = '$projectFolder'
	                and		p.name = '$projectName'
	                and		f.[name] = '$projectFolder';

	                if @ref > -1
	                begin
		                exec [catalog].[delete_environment_reference] @reference_id = @ref; 
	                end

	                exec [catalog].[delete_environment] @folder_name = '$projectFolder', @environment_name = '$projectFolder';
                end		

                if exists 
                (
	                select	1 
	                from	[internal].[projects] p
			                inner join [internal].[folders] f on p.folder_id = f.folder_id
	                where	p.name = '$projectName'
	                and		f.[name] = '$projectFolder'
                )
                begin
	                exec [catalog].[delete_project] @folder_name = '$projectFolder', @project_name = 'CHSP Dashboard Refresh';
                end"
    
    Invoke-Sqlcmd -ServerInstance $ssisServer -Query $initCMD -Database $ssisdb


    # Look for ispac and deploy. Should only be 1. 
    $ispacPath = "$projectFile\bin\$buildConfiguration".Substring(6) -replace "$projectName.dtproj", "" #remove the first ..\..\ set-Location at the beginning of the script is already at the top level    
    
    Get-ChildItem "$ispacPath" -Filter *.ispac | Foreach-Object {
        # $content = Get-Content $_.FullName
        $ispacFile = $_.FullName       
        # $_.BaseName  
        
        write-host "Found and will upload $ispacFile"
        [byte[]] $rawFile = [System.IO.File]::ReadAllBytes($ispacFile)
        $catalog = $integrationServices.Catalogs[$ssisdb] 
        $folder = $catalog.Folders[$projectFolder]
        $folder.DeployProject($projectName, $rawFile)     
        
    }    

    write-host "Creating environment $projectFolder and referencing to project $projectName"
    $postCMD = "declare @ref int; 
                exec [catalog].[create_environment] @folder_name = '$projectFolder', @environment_name = '$projectFolder';
                exec [catalog].[create_environment_reference] @environment_name = '$projectFolder', @project_name = '$projectName', @folder_name = '$projectFolder', @reference_type=R, @reference_id = @ref OUTPUT;
                "

    Invoke-Sqlcmd -ServerInstance $ssisServer -Query $postCMD -Database $ssisdb

    # Create environment variable if ISCatalogEnvironment.xml is found in Build folder. 
    configEnvironment -projectFolder $projectFolder -solution $solution -projectName $projectName -buildConfiguration $buildConfiguration
}



function New-SSRSWebServiceProxy
{
	param (
		[parameter(Mandatory=$true)]
		[ValidatePattern('^https?://')]
		[string]$Uri
		,[System.Management.Automation.PSCredential]$Credential
	)

	$script:ErrorActionPreference = 'Stop'
	Set-StrictMode -Version Latest

	$Assembly = [AppDomain]::CurrentDomain.GetAssemblies() | Where-Object {$_.GetType('SSRS.ReportingService2010.ReportingService2010')}

	if (($Assembly | Measure-Object).Count -gt 1) 
    {
		throw 'AppDomain contains multiple definitions of the same type. Restart PowerShell host.'
	}

	if (-not $Assembly) 
    {
		if ($Credential) 
        {
			$CredParams = @{ Credential = $Credential }
		} else {
			$CredParams = @{ UseDefaultCredential = $true }
		}
        
		$Proxy = New-WebServiceProxy -Uri $Uri -Namespace SSRS.ReportingService2010 @CredParams
	} 
    else 
    {
		$Proxy = New-Object -TypeName SSRS.ReportingService2010.ReportingService2010
		if ($Credential) 
        {
			$Proxy.Credentials = $Credential.GetNetworkCredential()
		} 
        else 
        {
			$Proxy.UseDefaultCredentials = $true
		}
	}

	$Proxy.Url = $Uri
	return $Proxy
}



function deploySSRS 
(
	[string]$projectName
    ,[string]$buildConfiguration	
    ,[string]$solutionPath
    ,[string]$projectFile
    ,[string]$solution
)
{
    # Clean up existing folder to ensure no residual issues when testing what is newly deployed. 
    write-host "Deploying $projectName project using $buildConfiguration configuration"    
    $ssrs = "http://az1-stgpbi01/ReportServer/ReportService2010.asmx?wsdl"
    $proxy = New-SSRSWebServiceProxy -Uri $ssrs

    foreach ($f in $proxy.ListChildren("/", $false))
    {
        if ($f.Name -eq "Build" -and $f.TypeName -eq "Folder")
        {
            foreach ($f in $proxy.ListChildren("/Build", $false))
            {
                if ($f.Name -eq $solution -and $f.TypeName -eq "Folder")
                {
                    $Proxy.DeleteItem("/Build/$solution")
                }
            } 
        }
    }     
    
    # Open up rptproj and make sure BUILD configuration has correct values. 
    [xml]$XmlDocument = Get-Content -Path $projectFile.Substring(6)

    foreach ($pg in $XmlDocument.Project.PropertyGroup)
    {
        if ($pg.Condition -match "BUILD")
        {
            $pg.TargetServerURL = "http://az1-stgpbi01/ReportServer"
            $pg.FullPath = "BUILD"
            $pg.OutputPath = "bin\BUILD"
            $pg.TargetReportFolder = "Build/$solution/Report"
            $pg.TargetDatasetFolder = "Build/$solution/DataSet"
            $pg.TargetDatasourceFolder = "Build/$solution/DataSource"
            $pg.TargetReportPartFolder = "Build/$solution/ReportParts"
            $pg.TargetServerVersion = "SSRS2016"
            $pg.OverwriteDatasets = "True"
            $pg.OverwriteDataSources = "True"
                
        }
    }
        
    $XmlDocument.Save($projectFile.Substring(6))

    # Deploy project. Details of where are recorded in project's configuration named BUILD. 
    $devEnv = "${env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\SQL\Common7\IDE\devenv.com"
    & "$devEnv" "$solutionPath" /Deploy $buildConfiguration /Project "$projectName"
}



# Set the working folder so that relative paths work consistently across build and development environments 
[Environment]::CurrentDirectory = (Split-Path $MyInvocation.MyCommand.Path -Parent ) -replace "\\Build", ""
[Environment]::CurrentDirectory

# Look through sln file and call the appropriate action depending on the project type
write-host "Reading $solutionPath"
Get-Content "$solutionPath" | Select-String 'Project\(' | ForEach-Object {      
    $projectParts = $_ -Split '[,=]' | ForEach-Object { $_.Trim('[ "{}]') };
      
    $projectName = $projectParts[1];
    $projectFile = $projectParts[2];
    $projectGuid = $projectParts[3];
      
    write-host "Found $projectName at $projectFile"      
    if ($projectFile -match ".dtproj") 
    {                
        Write-Host "This is a dtproj file"
        deployISPAC -projectFile $projectFile -solution $solution -projectName $projectName -buildConfiguration $buildConfiguration
    }
    elseif ($projectFile -match ".rptproj") 
    {                
        Write-Host "This is a rptproj file"
        deploySSRS -solutionPath $solutionPath -buildConfiguration $buildConfiguration -projectName $projectName -projectFile $projectFile -solution $solution
    }
    else 
    {           
        Write-Host "Unknown project, will not be worked on"        
    }
}
