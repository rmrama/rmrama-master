﻿CREATE TABLE [RDS].[BranchBorrowVisits] (
    [DeliveryVisitId]         VARCHAR (14)   NULL,
    [VisitStatus]             VARCHAR (1)    NULL,
    [VisitDate]               DATE           NULL,
    [VisitStart]              DATETIME       NULL,
    [VisitStop]               DATETIME       NULL,
    [Payable]                 VARCHAR (1)    NULL,
    [Billable]                VARCHAR (1)    NULL,
    [EpisodeCode]             VARCHAR (10)   NULL,
    [EpisodeName]             VARCHAR (40)   NULL,
    [OwningBranch]            VARCHAR (14)   NULL,
    [OwningBranchKey]         INT            NOT NULL,
    [ClientID]                VARCHAR (14)   NULL,
    [ClientKey]               INT            NOT NULL,
    [ClientFirstName]         VARCHAR (20)   NULL,
    [ClientLastName]          VARCHAR (30)   NULL,
    [Emp_ID]                  VARCHAR (14)   NULL,
    [employeeFirstname]       VARCHAR (20)   NULL,
    [employeeLastname]        VARCHAR (30)   NULL,
    [EmpKey]                  INT            NOT NULL,
    [DeliveryBranch]          VARCHAR (14)   NULL,
    [DeliveryBranchKey]       INT            NOT NULL,
    [OrderId]                 VARCHAR (14)   NULL,
    [OwningVisitID]           VARCHAR (14)   NULL,
    [OwningClientId]          VARCHAR (14)   NULL,
    [OwningClientKey]         INT            NOT NULL,
    [OwningEmpID]             VARCHAR (14)   NULL,
    [FundingSource]           VARCHAR (30)   NULL,
    [OwningVisitStatus]       VARCHAR (1)    NULL,
    [OwningVisitStart]        DATETIME       NULL,
    [OwningVisitStop]         DATETIME       NULL,
    [OwningClientFirstName]   VARCHAR (30)   NULL,
    [OwningClientLastName]    VARCHAR (30)   NULL,
    [OwningVisitPayable]      VARCHAR (1)    NULL,
    [OwningVisitBillable]     VARCHAR (1)    NULL,
    [OwningemployeeFirstname] VARCHAR (30)   NULL,
    [OwningemployeeLastname]  VARCHAR (30)   NULL,
    [OwningFundingSource]     VARCHAR (30)   NULL,
    [Pass]                    DECIMAL (3, 1) NULL,
    [ServType]                VARCHAR (10)   NULL,
    [OwningVisitServType]     VARCHAR (30)   NULL,
    [OwningEpisodeCode]       VARCHAR (30)   NULL,
    [OwningVisitBSOUT]        INT            NOT NULL,
    [BSOUT]                   INT            NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_BranchBorrowVisits_Visitid]
    ON [RDS].[BranchBorrowVisits]([DeliveryVisitId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_BranchBorrowVisits_OwningVisitid]
    ON [RDS].[BranchBorrowVisits]([OwningVisitID] ASC);

