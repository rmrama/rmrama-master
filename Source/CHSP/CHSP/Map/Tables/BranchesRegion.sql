﻿CREATE TABLE [Map].[BranchesRegion] (
    [BranchAlternateKey]   NVARCHAR (255) NULL,
    [RosterOnAlternateKey] FLOAT (53)     NULL,
    [PrecedaAlternateKey]  NVARCHAR (255) NULL,
    [TM1AlternateKey]      NVARCHAR (255) NULL,
    [Department]           NVARCHAR (255) NULL,
    [Region]               NVARCHAR (255) NULL,
    [LPA]                  NVARCHAR (50)  NULL,
    [Branch]               NVARCHAR (255) NULL,
    [State]                VARCHAR (20)   NULL,
    [ATSI]                 INT            NULL,
    [DateOn]               DATETIME       NULL,
    [DateOff]              DATETIME       NULL,
    [CreatedBy]            VARCHAR (100)  DEFAULT (suser_name()) NOT NULL,
    [CreatedDate]          DATETIME       DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]           VARCHAR (100)  DEFAULT (suser_name()) NULL,
    [ModifiedDate]         DATETIME       DEFAULT (getdate()) NULL,
    [Emp_ID]               NVARCHAR (255) NULL
);

