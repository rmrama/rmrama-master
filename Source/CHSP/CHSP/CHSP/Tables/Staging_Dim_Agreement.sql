﻿CREATE TABLE [CHSP].[Staging_Dim_Agreement] (
    [CHSPAgreementType]      VARCHAR (20)  NULL,
    [CHSPAgreementTypeDesc]  VARCHAR (100) NULL,
    [DisplayOrder]           INT           NULL,
    [CHSPAgreementTypeGroup] VARCHAR (100) NULL
);

