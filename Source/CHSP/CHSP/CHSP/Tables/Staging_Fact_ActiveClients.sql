﻿CREATE TABLE [CHSP].[Staging_Fact_ActiveClients] (
    [VisitID]            VARCHAR (14) NULL,
    [ClientID]           VARCHAR (14) NULL,
    [ClientDOB]          DATETIME     NULL,
    [VisitStartDate]     DATETIME     NULL,
    [VisitEndDate]       DATETIME     NULL,
    [Age]                INT          NULL,
    [FunderID]           VARCHAR (14) NULL,
    [FunderCode]         VARCHAR (20) NULL,
    [FunderName]         VARCHAR (30) NULL,
    [BranchCode]         VARCHAR (20) NULL,
    [BillRecDescription] VARCHAR (50) NULL,
    [OrderDescription]   VARCHAR (50) NULL,
    [CurrentPostalCode]  VARCHAR (10) NULL,
    [ATSIClient]         VARCHAR (1)  NULL,
    [ServiceType]        VARCHAR (10) NULL
);

