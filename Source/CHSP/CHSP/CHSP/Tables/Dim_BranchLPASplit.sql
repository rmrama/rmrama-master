﻿CREATE TABLE [CHSP].[Dim_BranchLPASplit] (
    [CHSPLPA]         VARCHAR (50)    NOT NULL,
    [BranchCode]      VARCHAR (20)    NOT NULL,
    [SplitPercentage] DECIMAL (38, 4) NULL,
    CONSTRAINT [PK_Dim_BranchLPASplit_1] PRIMARY KEY CLUSTERED ([BranchCode] ASC)
);

