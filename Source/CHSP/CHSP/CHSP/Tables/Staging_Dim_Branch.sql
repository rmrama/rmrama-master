﻿CREATE TABLE [CHSP].[Staging_Dim_Branch] (
    [CHSPLPA]            VARCHAR (20)  NULL,
    [BranchAlternateKey] VARCHAR (20)  NULL,
    [Department]         VARCHAR (100) NULL,
    [Region]             VARCHAR (100) NULL,
    [Branch]             VARCHAR (100) NULL,
    [CostCentre]         VARCHAR (20)  NULL,
    [Distribution]       VARCHAR (20)  NULL,
    [Entity]             VARCHAR (20)  NULL,
    [State]              VARCHAR (20)  NULL,
    [ATSI]               BIT           NULL,
    [DisplayOrder]       INT           NULL
);

