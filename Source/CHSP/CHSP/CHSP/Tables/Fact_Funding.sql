﻿CREATE TABLE [CHSP].[Fact_Funding] (
    [FinancialYear]         INT             NOT NULL,
    [AgreementLPA]          VARCHAR (50)    NOT NULL,
    [BranchKey]             INT             NULL,
    [ServiceTypeKey]        INT             NULL,
    [CHSPLPAKey]            INT             NULL,
    [CHSPAgreementTypeKey]  INT             NULL,
    [BranchCode]            VARCHAR (20)    NOT NULL,
    [ServiceType]           VARCHAR (100)   NOT NULL,
    [UnitType]              VARCHAR (50)    NULL,
    [FullYear_Funded_Units] DECIMAL (38, 4) NULL,
    [CHSPAgreement]         VARCHAR (20)    NULL,
    [ModifiedDate]          DATETIME        NULL,
    CONSTRAINT [PK_Fact_Funding_1] PRIMARY KEY CLUSTERED ([FinancialYear] ASC, [AgreementLPA] ASC, [BranchCode] ASC, [ServiceType] ASC)
);

