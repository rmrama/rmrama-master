﻿CREATE TABLE [CHSP].[Fact_AgreementTargets] (
    [CHSPAgreement]   VARCHAR (25)   NOT NULL,
    [TargetRange]     DECIMAL (9, 4) NULL,
    [RedRangeMin1]    DECIMAL (9, 4) NULL,
    [RedRangeMax1]    DECIMAL (9, 4) NULL,
    [YellowRangeMin1] DECIMAL (9, 4) NULL,
    [YellowRangeMax1] DECIMAL (9, 4) NULL,
    [GreenRangeMin]   DECIMAL (9, 4) NULL,
    [GreenRangeMax]   DECIMAL (9, 4) NULL,
    [YellowRangeMin2] DECIMAL (9, 4) NULL,
    [YellowRangeMax2] DECIMAL (9, 4) NULL,
    [RedRangeMin2]    DECIMAL (9, 4) NULL,
    [RedRangeMax2]    DECIMAL (9, 4) NULL,
    [Minimum]         DECIMAL (9, 4) NULL,
    [Maximum]         DECIMAL (9, 4) NULL,
    CONSTRAINT [PK_Fact_AgreementTargets] PRIMARY KEY CLUSTERED ([CHSPAgreement] ASC)
);

