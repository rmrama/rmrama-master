﻿CREATE TABLE [CHSP].[Dim_BranchMapping] (
    [CostCentreFrom] VARCHAR (20) NOT NULL,
    [CostCentreTo]   VARCHAR (20) NULL,
    [ATSIOnly]       VARCHAR (1)  NULL,
    CONSTRAINT [PK_Dim_BranchMapping] PRIMARY KEY CLUSTERED ([CostCentreFrom] ASC)
);

