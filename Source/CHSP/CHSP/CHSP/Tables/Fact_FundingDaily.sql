﻿CREATE TABLE [CHSP].[Fact_FundingDaily] (
    [FundingDateKey]       INT             NOT NULL,
    [FinancialYear]        INT             NOT NULL,
    [Funder]               VARCHAR (20)    NULL,
    [BranchKey]            INT             NOT NULL,
    [CHSPLPAKey]           INT             NOT NULL,
    [ServiceTypeKey]       INT             NOT NULL,
    [noofWeekends_Month]   INT             NULL,
    [noofWeekdays_Month]   INT             NULL,
    [noofWeekends_Year]    INT             NULL,
    [noofWeekdays_Year]    INT             NULL,
    [FullYear_FundedHours] DECIMAL (38, 4) NULL,
    [WDHours]              DECIMAL (38, 4) NULL,
    [WEHours]              DECIMAL (38, 4) NULL,
    [Funded_Hours]         DECIMAL (38, 4) NULL,
    [CHSPAgreementTypeKey] INT             NULL,
    [ModifiedDate]         DATETIME        NULL,
    CONSTRAINT [PK_Fact_FundingDaily_1] PRIMARY KEY CLUSTERED ([FundingDateKey] ASC, [FinancialYear] ASC, [BranchKey] ASC, [CHSPLPAKey] ASC, [ServiceTypeKey] ASC)
);

