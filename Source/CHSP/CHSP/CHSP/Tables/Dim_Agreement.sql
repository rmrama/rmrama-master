﻿CREATE TABLE [CHSP].[Dim_Agreement] (
    [CHSPAgreementTypeKey]   INT           IDENTITY (1, 1) NOT NULL,
    [CHSPAgreementType]      VARCHAR (20)  NULL,
    [CHSPAgreementTypeDesc]  VARCHAR (100) NULL,
    [DisplayOrder]           INT           NULL,
    [CHSPAgreementTypeGroup] VARCHAR (100) NULL,
    CONSTRAINT [PK_DimCHSP_FundingCHSPAgreement_CHSPAgreementTypeKey] PRIMARY KEY CLUSTERED ([CHSPAgreementTypeKey] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [NonClusteredIndex-20190404-100523]
    ON [CHSP].[Dim_Agreement]([CHSPAgreementType] ASC);

