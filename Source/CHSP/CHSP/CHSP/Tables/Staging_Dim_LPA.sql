﻿CREATE TABLE [CHSP].[Staging_Dim_LPA] (
    [CHSPLPA]       VARCHAR (50)  NULL,
    [CHSPLPADesc]   VARCHAR (100) NULL,
    [DisplayOrder]  INT           NULL,
    [CHSPAgreement] VARCHAR (100) NULL
);

