﻿CREATE TABLE [CHSP].[Staging_Fact_Funding] (
    [FinancialYear] INT             NULL,
    [AgreementLPA]  VARCHAR (50)    NULL,
    [ServiceType]   VARCHAR (100)   NULL,
    [Units]         DECIMAL (38, 4) NULL,
    [UnitType]      VARCHAR (50)    NULL,
    [Branch]        VARCHAR (50)    NULL,
    [BranchCode]    VARCHAR (20)    NULL
);

