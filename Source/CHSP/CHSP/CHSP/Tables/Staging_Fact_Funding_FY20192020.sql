﻿CREATE TABLE [CHSP].[Staging_Fact_Funding_FY20192020] (
    [ServiceType]   VARCHAR (100)   NULL,
    [AgreementLPA]  VARCHAR (50)    NULL,
    [UnitType]      VARCHAR (50)    NULL,
    [Units]         DECIMAL (38, 4) NULL,
    [CHSPAgreement] VARCHAR (50)    NULL
);

