﻿CREATE TABLE [CHSP].[Fact_ServiceHours] (
    [VisitID]             VARCHAR (14)    NOT NULL,
    [ClientID]            VARCHAR (14)    NOT NULL,
    [ClientDOB]           DATETIME        NULL,
    [EmployeeID]          VARCHAR (14)    NULL,
    [ServiceStartDate]    DATETIME        NULL,
    [ServiceEndDate]      DATETIME        NULL,
    [FunderID]            VARCHAR (14)    NULL,
    [FunderCode]          VARCHAR (20)    NULL,
    [FunderName]          VARCHAR (30)    NULL,
    [BranchCodeOriginal]  VARCHAR (20)    NULL,
    [BranchCode]          VARCHAR (20)    NULL,
    [ServiceTypeOriginal] VARCHAR (50)    NULL,
    [ServiceType]         VARCHAR (50)    NULL,
    [BillRecDescription]  VARCHAR (50)    NULL,
    [OrderDescription]    VARCHAR (50)    NULL,
    [BranchKey]           INT             NULL,
    [ServiceTypeKey]      INT             NULL,
    [CHSPLPAKey]          INT             NULL,
    [ServiceHoursRaw]     DECIMAL (38, 4) NULL,
    [ServiceHours]        DECIMAL (38, 4) NULL,
    [ServiceHours3060]    DECIMAL (38, 4) NULL,
    [SupervisoryHours]    DECIMAL (38, 4) NULL,
    [CurrentPostalCode]   VARCHAR (10)    NULL,
    [ATSIClient]          BIT             NULL,
    [IsLastVisit]         BIT             NULL,
    [Back2BackStart]      BIT             NULL,
    [Back2BackEnd]        BIT             NULL,
    [NextVisitStart]      DATETIME        NULL,
    [PreviousVisitEnd]    DATETIME        NULL,
    [BillUnits]           DECIMAL (38, 4) NULL,
    CONSTRAINT [PK_Fact_ServiceHours] PRIMARY KEY NONCLUSTERED ([VisitID] ASC, [ClientID] ASC)
);


GO
CREATE CLUSTERED INDEX [ClusteredIndex-20190411-132616]
    ON [CHSP].[Fact_ServiceHours]([VisitID] ASC, [ClientID] ASC, [ServiceStartDate] ASC);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20190411-132801]
    ON [CHSP].[Fact_ServiceHours]([ServiceStartDate] ASC, [BranchKey] ASC, [ServiceTypeKey] ASC, [CHSPLPAKey] ASC)
    INCLUDE([ClientID], [ServiceHoursRaw], [ServiceHours], [FunderName]);

