﻿CREATE TABLE [CHSP].[Dim_PublicHolidays] (
    [DateKey]              INT           NOT NULL,
    [FullDateAlternateKey] DATE          NULL,
    [HolidayName]          VARCHAR (255) NOT NULL,
    [ApplicableTo]         VARCHAR (255) NULL,
    [IsApplicableTo_NT]    VARCHAR (1)   NULL,
    [IsApplicableTo_WA]    VARCHAR (1)   NULL,
    [IsApplicableTo_VIC]   VARCHAR (1)   NULL,
    [IsApplicableTo_TAS]   VARCHAR (1)   NULL,
    [IsApplicableTo_NSW]   VARCHAR (1)   NULL,
    [IsApplicableTo_SA]    VARCHAR (1)   NULL,
    [IsApplicableTo_ACT]   VARCHAR (1)   NULL,
    [IsApplicableTo_QLD]   VARCHAR (1)   NULL,
    CONSTRAINT [PK_Dim_PublicHolidays] PRIMARY KEY CLUSTERED ([DateKey] ASC, [HolidayName] ASC)
);

