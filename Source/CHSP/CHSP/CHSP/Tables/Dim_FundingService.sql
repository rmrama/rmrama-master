﻿CREATE TABLE [CHSP].[Dim_FundingService] (
    [ServiceTypeKey]   INT           IDENTITY (1, 1) NOT NULL,
    [ServiceType]      VARCHAR (50)  NULL,
    [ServiceTypeDesc]  VARCHAR (100) NULL,
    [DisplayOrder]     INT           NULL,
    [ServiceTypeGroup] VARCHAR (100) NULL,
    [ToDashboard]      BIT           NULL,
    CONSTRAINT [PK_DimCHSP_FundingService_ServiceTypeKey] PRIMARY KEY CLUSTERED ([ServiceTypeKey] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [NonClusteredIndex-20190404-100629]
    ON [CHSP].[Dim_FundingService]([ServiceType] ASC);

