﻿CREATE TABLE [CHSP].[Dim_Branch] (
    [BranchKey]          INT           IDENTITY (1, 1) NOT NULL,
    [LPAKey]             INT           NOT NULL,
    [BranchAlternateKey] VARCHAR (20)  NULL,
    [Department]         VARCHAR (100) NULL,
    [Region]             VARCHAR (100) NULL,
    [Branch]             VARCHAR (100) NULL,
    [CostCentre]         VARCHAR (20)  NULL,
    [Distribution]       VARCHAR (20)  NULL,
    [Entity]             VARCHAR (20)  NULL,
    [State]              VARCHAR (20)  NULL,
    [ATSI]               BIT           NULL,
    [DisplayOrder]       INT           NULL,
    CONSTRAINT [PK_DimCHSPBranch_BranchKey] PRIMARY KEY CLUSTERED ([BranchKey] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [NonClusteredIndex-20190404-100449]
    ON [CHSP].[Dim_Branch]([CostCentre] ASC);

