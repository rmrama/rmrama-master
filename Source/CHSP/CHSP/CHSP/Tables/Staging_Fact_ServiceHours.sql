﻿CREATE TABLE [CHSP].[Staging_Fact_ServiceHours] (
    [ServiceStartDate]   DATETIME        NULL,
    [ServiceEndDate]     DATETIME        NULL,
    [ClientID]           VARCHAR (14)    NULL,
    [ClientDOB]          DATETIME        NULL,
    [VisitID]            VARCHAR (14)    NULL,
    [EmployeeID]         VARCHAR (14)    NULL,
    [BranchCode]         VARCHAR (20)    NULL,
    [FunderID]           VARCHAR (14)    NULL,
    [FunderCode]         VARCHAR (20)    NULL,
    [FunderName]         VARCHAR (30)    NULL,
    [VerificationStatus] VARCHAR (10)    NULL,
    [VisitStatus]        VARCHAR (10)    NULL,
    [ServiceHoursRaw]    DECIMAL (38, 4) NULL,
    [BillRecDescription] VARCHAR (50)    NULL,
    [OrderDescription]   VARCHAR (50)    NULL,
    [CurrentPostalCode]  VARCHAR (10)    NULL,
    [ATSIClient]         VARCHAR (1)     NULL,
    [ServiceType]        VARCHAR (10)    NULL,
    [BillUnits]          DECIMAL (38, 4) NULL
);

