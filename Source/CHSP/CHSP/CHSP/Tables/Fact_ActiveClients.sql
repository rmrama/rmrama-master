﻿CREATE TABLE [CHSP].[Fact_ActiveClients] (
    [VisitID]             VARCHAR (14) NOT NULL,
    [ClientID]            VARCHAR (14) NOT NULL,
    [ClientDOB]           DATETIME     NULL,
    [VisitStartDate]      DATETIME     NOT NULL,
    [VisitEndDate]        DATETIME     NOT NULL,
    [FunderID]            VARCHAR (14) NULL,
    [FunderCode]          VARCHAR (20) NULL,
    [FunderName]          VARCHAR (30) NULL,
    [BranchCodeOriginal]  VARCHAR (20) NULL,
    [BranchCode]          VARCHAR (20) NULL,
    [ServiceTypeOriginal] VARCHAR (50) NULL,
    [ServiceType]         VARCHAR (50) NULL,
    [ReportingMonth]      DATE         NOT NULL,
    [BranchKey]           INT          NOT NULL,
    [ServiceTypeKey]      INT          NOT NULL,
    [CHSPLPAKey]          INT          NOT NULL,
    [BillRecDescription]  VARCHAR (50) NULL,
    [OrderDescription]    VARCHAR (50) NULL,
    [CurrentPostalCode]   VARCHAR (10) NULL,
    [ATSIClient]          BIT          NULL,
    CONSTRAINT [PK_Fact_ActiveClients_1] PRIMARY KEY CLUSTERED ([VisitID] ASC, [ClientID] ASC, [ReportingMonth] ASC)
);

