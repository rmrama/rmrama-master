﻿CREATE TABLE [CHSP].[Staging_Fact_ServiceHoursSupervisory] (
    [ClientID]      VARCHAR (14) NULL,
    [DepartmentID]  VARCHAR (14) NULL,
    [VisitID]       VARCHAR (14) NULL,
    [DueDate]       DATETIME     NULL,
    [CompletedDate] DATETIME     NULL,
    [BranchCode]    VARCHAR (20) NULL,
    [ATSIClient]    VARCHAR (1)  NULL,
    [Requirement]   VARCHAR (14) NULL,
    [ReqID]         VARCHAR (14) NULL
);

