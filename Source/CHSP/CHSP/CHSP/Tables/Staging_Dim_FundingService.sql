﻿CREATE TABLE [CHSP].[Staging_Dim_FundingService] (
    [ServiceType]      VARCHAR (50)  NULL,
    [ServiceTypeDesc]  VARCHAR (100) NULL,
    [DisplayOrder]     INT           NULL,
    [ServiceTypeGroup] VARCHAR (100) NULL
);

