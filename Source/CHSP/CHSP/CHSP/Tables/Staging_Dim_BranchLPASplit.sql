﻿CREATE TABLE [CHSP].[Staging_Dim_BranchLPASplit] (
    [CHSPLPA]         VARCHAR (50)    NULL,
    [BranchCode]      VARCHAR (20)    NULL,
    [SplitPercentage] DECIMAL (38, 4) NULL
);

