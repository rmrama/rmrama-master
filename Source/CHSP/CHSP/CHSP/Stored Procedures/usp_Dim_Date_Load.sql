﻿



CREATE procedure [CHSP].[usp_Dim_Date_Load] 
as
-- exec [CHSP].[usp_Dim_Date_Load]
-- =================================================================================================================
--
-- Author:			M.Rama
--
-- Description:		Upkeeps date dimension from a central source
--
-- Purpose:			Various uses
--					
--
-- Run Frequency:	TBD
--
-- =================
-- Revision History:
-- =================								 			
--
-- Version		Date		Who		Description		
-- ----------------------------------------------------------------------------------------------------------------
--  1.0			03/04/2019	M.Rama		Initial
-- =================================================================================================================


-- =================================================================================================================
-- Turn off messages SQL sends back to the client after each TSQL Statement
-- =================================================================================================================
set nocount on;


-- =================================================================================================================
-- Merge into target
-- =================================================================================================================
merge	[CHSP].[Dim_Date] t
using	[CHSP].[Staging_Dim_Date] r 
		on (t.[DateKey] = r.[DateKey])

when	matched then update set
			t.[DateKey] = r.[DateKey]
			,t.[FullDateAlternateKey] = r.[FullDateAlternateKey]
			,t.[DayNumberOfWeek] = r.[DayNumberOfWeek]
			,t.[EnglishDayNameOfWeek] = r.[EnglishDayNameOfWeek]
			,t.[EnglishDayNameOfWeekShort] = r.[EnglishDayNameOfWeekShort]
			,t.[Month] = r.[Month]
			,t.[EnglishMonthName] = r.[EnglishMonthName]
			,t.[EnglishMonthNameShort] = r.[EnglishMonthNameShort]
			,t.[EnglishMonthYear] = r.[EnglishMonthYear]
			,t.[EnglishMonthYear_mmm-yy] = r.[EnglishMonthYear_mmm-yy]
			,t.[EnglishMonthYear_mmm-yyyy] = r.[EnglishMonthYear_mmm-yyyy]
			,t.[CalendarQuarter] = r.[CalendarQuarter]
			,t.[CalendarQuarterName] = r.[CalendarQuarterName]
			,t.[CalendarQuarterNameFull] = r.[CalendarQuarterNameFull]
			,t.[CalendarQuarterNameShort] = r.[CalendarQuarterNameShort]
			,t.[FinancialQuarter] = r.[FinancialQuarter]
			,t.[FinancialQuarterName] = r.[FinancialQuarterName]
			,t.[FinancialQuarterNameFull] = r.[FinancialQuarterNameFull]
			,t.[FinancialQuarterNameShort] = r.[FinancialQuarterNameShort]
			,t.[FinancialYearQuarterName] = r.[FinancialYearQuarterName]
			,t.[FinancialYearQuarterNameFull] = r.[FinancialYearQuarterNameFull]
			,t.[FinancialYearQuarterName_FYyyyy QR] = r.[FinancialYearQuarterName_FYyyyy QR]
			,t.[FinancialYearQuarterName_yyyy-QR] = r.[FinancialYearQuarterName_yyyy-QR]
			,t.[FinancialYear] = r.[FinancialYear]
			,t.[FinancialYearName] = r.[FinancialYearName]
			,t.[EnglishFullName] = r.[EnglishFullName]
			,t.[Day] = r.[Day]
			,t.[DayWithSuffix] = r.[DayWithSuffix]
			,t.[Year] = r.[Year]
			,t.[Date_yyyy-mm] = r.[Date_yyyy-mm]
			,t.[Date_yyyymm] = r.[Date_yyyymm]
			,t.[Date_dd/mm/yyyy] = r.[Date_dd/mm/yyyy]
			,t.[FirstDayOfMonth] = r.[FirstDayOfMonth]
			,t.[LastDayOfMonth] = r.[LastDayOfMonth]
			,t.[FirstDayOfMonth_dd/mm/yy] = r.[FirstDayOfMonth_dd/mm/yy]
			,t.[LastDayOfMonth_dd/mm/yy] = r.[LastDayOfMonth_dd/mm/yy]
			,t.[DaysInMonth] = r.[DaysInMonth]
			,t.[FirstDayOfFinancialQuarter] = r.[FirstDayOfFinancialQuarter]
			,t.[LastDayOfFinancialQuarter] = r.[LastDayOfFinancialQuarter]
			,t.[FirstDayOfFinancialYear] = r.[FirstDayOfFinancialYear]
			,t.[LastDayOfFinancialYear] = r.[LastDayOfFinancialYear]
			,t.[AUMembershipSnap] = r.[AUMembershipSnap]
			,t.[ReportMartOrigDateKey] = r.[ReportMartOrigDateKey]
			,t.[HealthClaimsBIOrigDateKey] = r.[HealthClaimsBIOrigDateKey]
			,t.[IsWeekend] = r.[IsWeekend]
			,t.[IsFirstDayOfMonth] = r.[IsFirstDayOfMonth]
			,t.[IsLastDayOfMonth] = r.[IsLastDayOfMonth]
			,t.[IsFirst_WeekDayOfMonth] = r.[IsFirst_WeekDayOfMonth]
			,t.[IsLast_WeekDayOfMonth] = r.[IsLast_WeekDayOfMonth]
			,t.[IsFirstDayOfFinancialYear] = r.[IsFirstDayOfFinancialYear]
			,t.[IsLastDayOfFinancialYear] = r.[IsLastDayOfFinancialYear]
			,t.[IALWeekNumber] = r.[IALWeekNumber]
			,t.[IALWeekStart] = r.[IALWeekStart]
			,t.[IALWeekEnd] = r.[IALWeekEnd]
			,t.[IALPayrollWeekStart] = r.[IALPayrollWeekStart]
			,t.[IALPayrollWeekEnd] = r.[IALPayrollWeekEnd]
			,t.[IALPayrollWeekNumber] = r.[IALPayrollWeekNumber]
			,t.[EnglishDayMonth_ddMMM] = r.[EnglishDayMonth_ddMMM]			

when	not matched then insert
		(
			[DateKey]
			,[FullDateAlternateKey]
			,[DayNumberOfWeek]
			,[EnglishDayNameOfWeek]
			,[EnglishDayNameOfWeekShort]
			,[Month]
			,[EnglishMonthName]
			,[EnglishMonthNameShort]
			,[EnglishMonthYear]
			,[EnglishMonthYear_mmm-yy]
			,[EnglishMonthYear_mmm-yyyy]
			,[CalendarQuarter]
			,[CalendarQuarterName]
			,[CalendarQuarterNameFull]
			,[CalendarQuarterNameShort]
			,[FinancialQuarter]
			,[FinancialQuarterName]
			,[FinancialQuarterNameFull]
			,[FinancialQuarterNameShort]
			,[FinancialYearQuarterName]
			,[FinancialYearQuarterNameFull]
			,[FinancialYearQuarterName_FYyyyy QR]
			,[FinancialYearQuarterName_yyyy-QR]
			,[FinancialYear]
			,[FinancialYearName]
			,[EnglishFullName]
			,[Day]
			,[DayWithSuffix]
			,[Year]
			,[Date_yyyy-mm]
			,[Date_yyyymm]
			,[Date_dd/mm/yyyy]
			,[FirstDayOfMonth]
			,[LastDayOfMonth]
			,[FirstDayOfMonth_dd/mm/yy]
			,[LastDayOfMonth_dd/mm/yy]
			,[DaysInMonth]
			,[FirstDayOfFinancialQuarter]
			,[LastDayOfFinancialQuarter]
			,[FirstDayOfFinancialYear]
			,[LastDayOfFinancialYear]
			,[AUMembershipSnap]
			,[ReportMartOrigDateKey]
			,[HealthClaimsBIOrigDateKey]
			,[IsWeekend]
			,[IsFirstDayOfMonth]
			,[IsLastDayOfMonth]
			,[IsFirst_WeekDayOfMonth]
			,[IsLast_WeekDayOfMonth]
			,[IsFirstDayOfFinancialYear]
			,[IsLastDayOfFinancialYear]
			,[IALWeekNumber]
			,[IALWeekStart]
			,[IALWeekEnd]
			,[IALPayrollWeekStart]
			,[IALPayrollWeekEnd]
			,[IALPayrollWeekNumber]
			,[EnglishDayMonth_ddMMM]			
		)
		values
		(
			r.[DateKey]
			,r.[FullDateAlternateKey]
			,r.[DayNumberOfWeek]
			,r.[EnglishDayNameOfWeek]
			,r.[EnglishDayNameOfWeekShort]
			,r.[Month]
			,r.[EnglishMonthName]
			,r.[EnglishMonthNameShort]
			,r.[EnglishMonthYear]
			,r.[EnglishMonthYear_mmm-yy]
			,r.[EnglishMonthYear_mmm-yyyy]
			,r.[CalendarQuarter]
			,r.[CalendarQuarterName]
			,r.[CalendarQuarterNameFull]
			,r.[CalendarQuarterNameShort]
			,r.[FinancialQuarter]
			,r.[FinancialQuarterName]
			,r.[FinancialQuarterNameFull]
			,r.[FinancialQuarterNameShort]
			,r.[FinancialYearQuarterName]
			,r.[FinancialYearQuarterNameFull]
			,r.[FinancialYearQuarterName_FYyyyy QR]
			,r.[FinancialYearQuarterName_yyyy-QR]
			,r.[FinancialYear]
			,r.[FinancialYearName]
			,r.[EnglishFullName]
			,r.[Day]
			,r.[DayWithSuffix]
			,r.[Year]
			,r.[Date_yyyy-mm]
			,r.[Date_yyyymm]
			,r.[Date_dd/mm/yyyy]
			,r.[FirstDayOfMonth]
			,r.[LastDayOfMonth]
			,r.[FirstDayOfMonth_dd/mm/yy]
			,r.[LastDayOfMonth_dd/mm/yy]
			,r.[DaysInMonth]
			,r.[FirstDayOfFinancialQuarter]
			,r.[LastDayOfFinancialQuarter]
			,r.[FirstDayOfFinancialYear]
			,r.[LastDayOfFinancialYear]
			,r.[AUMembershipSnap]
			,r.[ReportMartOrigDateKey]
			,r.[HealthClaimsBIOrigDateKey]
			,r.[IsWeekend]
			,r.[IsFirstDayOfMonth]
			,r.[IsLastDayOfMonth]
			,r.[IsFirst_WeekDayOfMonth]
			,r.[IsLast_WeekDayOfMonth]
			,r.[IsFirstDayOfFinancialYear]
			,r.[IsLastDayOfFinancialYear]
			,r.[IALWeekNumber]
			,r.[IALWeekStart]
			,r.[IALWeekEnd]
			,r.[IALPayrollWeekStart]
			,r.[IALPayrollWeekEnd]
			,r.[IALPayrollWeekNumber]
			,r.[EnglishDayMonth_ddMMM]	
		)
;

