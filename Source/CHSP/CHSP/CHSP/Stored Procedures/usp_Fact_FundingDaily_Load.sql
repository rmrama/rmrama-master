﻿

CREATE procedure [CHSP].[usp_Fact_FundingDaily_Load] 
(
	@FinancialYear int = NULL
) 
as
-- exec [CHSP].[usp_Fact_FundingDaily_Load] @FinancialYear = 2019
-- =================================================================================================================
--
-- Author:			M.Rama
--
-- Description:		Spreads out yearly funding to daily level
--
-- Purpose:			Spread data will service all reporting requirements
--					
--
-- Run Frequency:	TBD
--
-- =================
-- Revision History:
-- =================								 			
--
-- Version		Date		Who		Description		
-- ----------------------------------------------------------------------------------------------------------------
--  1.0			03/04/2019	M.Rama		Initial
-- =================================================================================================================


-- =================================================================================================================
-- Turn off messages SQL sends back to the client after each TSQL Statement
-- =================================================================================================================
set nocount on;


-- =================================================================================================================
-- Localize parameter
-- =================================================================================================================
declare @IFinancialYear int = @FinancialYear; 
--declare @IFinancialYear int = 2018; 
declare @ModifiedDate datetime = getdate(); 
declare @StartDate date = convert(varchar, @IFinancialYear - 1) + '-07-01';
declare @EndDate date = convert(varchar, @IFinancialYear) + '-06-30';

-- =================================================================================================================
-- Prepare working tables
-- =================================================================================================================
if object_id('tempdb..#PublicHolidays') is not null drop table #PublicHolidays; 
if object_id('tempdb..#WorkDaySummaryMonth') is not null drop table #WorkDaySummaryMonth; 
if object_id('tempdb..#WorkDaySummary') is not null drop table #WorkDaySummary; 
if object_id('tempdb..#Result') is not null drop table #Result; 
if object_id('tempdb..#DimDate') is not null drop table #DimDate; 

create table #DimDate 
(
	FinancialYear nvarchar(4)
	,Month tinyint
	,DateKey int
	,FullDateAlternateKey date
	,FirstDayOfMonth date
	,IsWeekend bit
)
;

while @StartDate < @EndDate
begin
	insert	into #DimDate
	select	@IFinancialYear, month(@StartDate), convert(int, convert(varchar, @StartDate, 112)), @StartDate, dateadd(day, -1 * (day(@StartDate) - 1), @StartDate), case when (datename(WEEKDAY, @StartDate) in ('Saturday', 'Sunday')) then 1 else 0 end
	;

	set @StartDate = DATEADD(day, 1, @StartDate); 
end


-- =================================================================================================================
-- Working and non working days by State for a FY. Create 2 summaries that will be used right at the end
-- =================================================================================================================
select	State, PublicHoliday
into	#PublicHolidays
from 
(
		select	'NT' as State, DateKey as PublicHoliday from [CHSP].[Dim_PublicHolidays] where [IsApplicableTo_NT] = 1
		union all
		select	'WA' as State, DateKey as PublicHoliday from [CHSP].[Dim_PublicHolidays] where [IsApplicableTo_WA] = 1
		union all
		select	'VIC' as State, DateKey as PublicHoliday from [CHSP].[Dim_PublicHolidays] where [IsApplicableTo_VIC] = 1
		union all
		select	'TAS' as State, DateKey as PublicHoliday from [CHSP].[Dim_PublicHolidays] where [IsApplicableTo_TAS] = 1
		union all
		select	'NSW' as State, DateKey as PublicHoliday from [CHSP].[Dim_PublicHolidays] where [IsApplicableTo_NSW] = 1
		union all
		select	'SA' as State, DateKey as PublicHoliday from [CHSP].[Dim_PublicHolidays] where [IsApplicableTo_SA] = 1
		union all
		select	'ACT' as State, DateKey as PublicHoliday from [CHSP].[Dim_PublicHolidays] where [IsApplicableTo_ACT] = 1
		union all
		select	'QLD' as State, DateKey as PublicHoliday from [CHSP].[Dim_PublicHolidays] where [IsApplicableTo_QLD] = 1
) a ; 

with	c_State
as
(
		select	'NT' as State
		union all
		select	'WA' as State
		union all
		select	'VIC' as State
		union all
		select	'TAS' as State
		union all
		select	'NSW' as State
		union all
		select	'SA' as State
		union all
		select	'ACT' as State
		union all
		select	'QLD' as State
)
select	st.State, dd.Month
		,sum(case when (IsWeekend = 0 and ph.PublicHoliday is null) then 1 else 0 end) WorkingDays
		,sum(case when (IsWeekend = 1 or ph.PublicHoliday is not null) then 1 else 0 end) NonWorkingDays
into	#WorkDaySummaryMonth
from	c_State st 
		inner join #DimDate dd on 1 = 1
		left join #PublicHolidays ph on ph.PublicHoliday = dd.DateKey and st.State = ph.State
where	dd.FinancialYear = @IFinancialYear
group by
		st.State, dd.Month; 

select	State
		,sum(WorkingDays) WorkingDays
		,sum(NonWorkingDays) NonWorkingDays
into	#WorkDaySummary
from	#WorkDaySummaryMonth
group by
		State; 

-- =================================================================================================================
-- Take funding for each FY and blow up to daily
-- Take working and non working days count based on the State of the branch. 
-- =================================================================================================================
select	dd.[DateKey] as [FundingDateKey]
		,f.[FinancialYear]
		,'CHSP' as Funder
		,f.[BranchKey]
		,f.[CHSPLPAKey]
		,f.[ServiceTypeKey]
		,wdm.NonWorkingDays [noofWeekends_Month]
		,wdm.WorkingDays [noofWeekdays_Month]
		,wd.NonWorkingDays [noofWeekends_Year]
		,wd.WorkingDays [noofWeekdays_Year]
		,f.[FullYear_Funded_Units] as [FullYear_FundedHours]
		,[WDHours] = convert(decimal(38, 4), (f.[FullYear_Funded_Units] * 0.95) / wd.WorkingDays)
		,[WEHours] = convert(decimal(38, 4), (f.[FullYear_Funded_Units] * 0.05) / wd.NonWorkingDays)
		,[Funded_Hours] = convert(decimal(38, 4), case when (dd.IsWeekend = 1 or ph.PublicHoliday is not null) then (f.[FullYear_Funded_Units] * 0.05) / wd.NonWorkingDays
							else (f.[FullYear_Funded_Units] * 0.95) / wd.WorkingDays end)
		,f.[CHSPAgreementTypeKey]
into	#Result
from	[CHSP].[Fact_Funding] f
		inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = f.[BranchKey]
		inner join #DimDate dd on 1 = 1
		inner join #WorkDaySummary wd on wd.State = br.State
		inner join #WorkDaySummaryMonth wdm on wdm.State = br.State and dd.Month = wdm.Month 
		left join #PublicHolidays ph on ph.PublicHoliday = dd.[DateKey] and br.State = ph.State
where	dd.FinancialYear = @IFinancialYear
and		f.FinancialYear = @IFinancialYear
and		f.[BranchKey] > 0
and		f.[ServiceTypeKey] > 0
and		f.[CHSPLPAKey] > 0
and		f.[CHSPAgreementTypeKey] > 0
and		f.[UnitType] = 'Hours'; 		


-- =================================================================================================================
-- Merge into target
-- =================================================================================================================
merge	[CHSP].[Fact_FundingDaily] t
using	#Result r 
		on (t.[BranchKey] = r.[BranchKey] and t.[CHSPLPAKey] = r.[CHSPLPAKey] and t.[ServiceTypeKey] = r.[ServiceTypeKey] and t.[FundingDateKey] = r.[FundingDateKey])

when	matched then update set		
		t.[FinancialYear] = r.[FinancialYear]
		,t.Funder = r.Funder
		,t.[noofWeekends_Month] = r.[noofWeekends_Month]
		,t.[noofWeekdays_Month] = r.[noofWeekdays_Month]
		,t.[noofWeekends_Year] = r.[noofWeekends_Year]
		,t.[noofWeekdays_Year] = r.[noofWeekdays_Year]
		,t.[FullYear_FundedHours] = r.[FullYear_FundedHours]
		,t.[WDHours] = r.[WDHours]
		,t.[WEHours] = r.[WEHours]
		,t.[Funded_Hours] = r.[Funded_Hours]
		,t.[CHSPAgreementTypeKey] = r.[CHSPAgreementTypeKey]
		,t.[ModifiedDate] = @ModifiedDate

when	not matched then insert
		(
			[FundingDateKey]
			,[FinancialYear]
			,[Funder]
			,[BranchKey]
			,[CHSPLPAKey]
			,[ServiceTypeKey]
			,[noofWeekends_Month]
			,[noofWeekdays_Month]
			,[noofWeekends_Year]
			,[noofWeekdays_Year]
			,[FullYear_FundedHours]
			,[WDHours]
			,[WEHours]
			,[Funded_Hours]
			,[CHSPAgreementTypeKey]
			,[ModifiedDate]
		)
		values
		(
			r.[FundingDateKey]
			,r.[FinancialYear]
			,r.[Funder]
			,r.[BranchKey]
			,r.[CHSPLPAKey]
			,r.[ServiceTypeKey]
			,r.[noofWeekends_Month]
			,r.[noofWeekdays_Month]
			,r.[noofWeekends_Year]
			,r.[noofWeekdays_Year]
			,r.[FullYear_FundedHours]
			,r.[WDHours]
			,r.[WEHours]
			,r.[Funded_Hours]
			,r.[CHSPAgreementTypeKey]
			,@ModifiedDate
		)
;


-- =================================================================================================================
-- Clean up orphans
-- =================================================================================================================
delete
from	[CHSP].[Fact_FundingDaily]
where	[FinancialYear] = @IFinancialYear
and		[ModifiedDate] <> @ModifiedDate; 


---- =================================================================================================================
---- Update the full funding numbers into the daily table
---- =================================================================================================================
--update	[CHSP].[Fact_FundingDaily]
--set		[FullYear_FundedHours] = x.FullYear_Funded_Hours
--from	[CHSP].[Fact_FundingDaily] t1
--		inner join [CHSP].[Fact_Funding] x
--			on x.Branch = t1.Branch
--			and x.BranchCode = t1.BranchCode
--			and x.AgreementLPA = t1.AgreementLPA
--			and x.ServiceType = t1.ServiceType
--where	x.FinancialYear = @IFinancialYear; 


---- =================================================================================================================
---- Recalculate the daily budget figures. Take working and non working days count based on the State of the branch
---- =================================================================================================================
--update	[CHSP].[Fact_FundingDaily]
--set		[WDHours] = ([FullYear_FundedHours] * 0.95) / WorkingDays
--		,[WEHours] = ([FullYear_FundedHours] * 0.05) / NonWorkingDays
--		,[Funded_Hours] = case when (dd.IsWeekend = 1 or ph.PublicHoliday is not null) then ([FullYear_FundedHours] * 0.05) / NonWorkingDays
--							else ([FullYear_FundedHours] * 0.95) / WorkingDays
--						end
--FROM	[CHSP].[Fact_FundingDaily] t1
--		inner join [CHSP].[Dim_Branch] br on br.[CostCentre] = t1.BranchCode
--		inner join [CHSP].[Dim_Date] dd on dd.FullDateAlternateKey = t1.FundingDay
--		inner join #WorkDaySummary wd on wd.State = br.State
--		left join #PublicHolidays ph on ph.PublicHoliday = t1.FundingDay;

