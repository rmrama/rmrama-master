﻿

CREATE procedure [CHSP].[usp_CHSPDEX_Output] 
(
	@FromDate	Date = null
	,@ToDate	Date = null
)
as
-- exec [CHSP].[usp_CHSPDEX_Output] @FromDate = '2019-01-01', @ToDate = '2019-06-30'
-- =================================================================================================================
--
-- Author:			M.Rama
--
-- Description:		Extracts service hours for DEX reporting
--
-- Purpose:			Reporting to external
--					
--
-- Run Frequency:	TBD
--
-- =================
-- Revision History:
-- =================								 			
--
-- Version		Date		Who		Description		
-- ----------------------------------------------------------------------------------------------------------------
--  1.0			05/06/2019	M.Rama		Initial
-- =================================================================================================================


-- =================================================================================================================
-- Turn off messages SQL sends back to the client after each TSQL Statement
-- =================================================================================================================
set nocount on;


-- =================================================================================================================
-- Localize parameter
-- =================================================================================================================
declare @IFromDate date = @FromDate; 
declare @IToDate date = @ToDate; 
declare @Timestamp varchar(100) = replace(replace(replace(replace(convert(varchar, getdate(), 121), '-', ''), ' ', ''), ':', ''), '.', ''); 
declare @RenameTable varchar(500) = null; 


-- =================================================================================================================
-- Prepare working tables
-- =================================================================================================================
if object_id ('tempdb..#Outlets2') is not null 
begin
	drop table #Outlets2; 
end

if object_id ('tempdb..#SHRaw') is not null 
begin
	drop table #SHRaw; 
end

if object_id ('tempdb..#disability') is not null 
begin
	drop table #disability; 
end

if object_id ('tempdb..#session') is not null 
begin
	drop table #session; 
end

if object_id ('tempdb..#case') is not null 
begin
	drop table #case; 
end

if object_id ('tempdb..#client') is not null 
begin
	drop table #client; 
end


-- =================================================================================================================
-- Populate lookups tables
-- =================================================================================================================
select	FUNDER_ID
		,cast([REFDATA] as XML).value('(/PropBag/Prop/@Name)[1]', 'varchar(100)') as Name
		,cast([REFDATA] as XML).value('(/PropBag/Prop/@Value)[1]', 'varchar(100)') as OutletactivityId
into	#Outlets2
from	Audb_IAL_Procura_Mart.[dbo].[FUNDERREFS] FREF
where	cast([REFDATA] as XML).value('(/PropBag/Prop/@Name)[1]', 'varchar(100)') = 'Outlet Activity ID'
; 


-- =================================================================================================================
-- This will populate DEX_Sessions and DEX_Cases and help start DEX_Clients and DEX_Disability off
-- =================================================================================================================
;with	c_ordersorig
as
(
		select	*
		from
		(
				select	sh.ClientID, brnch.BranchKey, convert(DATE, ServiceStartDate) as ServiceStartDate, o.ORDER_ID, o.FUNDER_ID
						,row_number() over (partition by sh.ClientID, sh.BranchKey, convert(DATE, ServiceStartDate) order by abs(datediff(day, o.STARTDATE, ServiceStartDate)) asc) as _RNum
				from	[CHSP].[Fact_ServiceHours] sh 
						inner join CHSP.Dim_Branch brnch on brnch.CostCentre = sh.BranchCodeOriginal
						inner join [Audb_IAL_Procura_Mart].[dbo].[ORDERS] o on sh.ClientID = o.CLIENT_ID and o.DEPT_ID = brnch.BranchAlternateKey
						inner join [Audb_IAL_Procura_Mart].[dbo].[FUNDERS] f on f.FUNDER_ID = o.FUNDER_ID											
				where	convert(date, [ServiceStartDate]) between @IFromDate and @IToDate
				and		SupervisoryHours is not null
				and		(f.CODE like 'CHSP%' or f.CODE like 'NRCP%' or f.code like 'HACC%' or f.code like 'CCSP%')				
		) a
		where	a._RNum = 1

)
,		c_orders
as
(
		select	*
		from
		(
				select	sh.ClientID, brnch.BranchKey, convert(DATE, ServiceStartDate) as ServiceStartDate, o.ORDER_ID, o.FUNDER_ID
						,row_number() over (partition by sh.ClientID, sh.BranchKey, convert(DATE, ServiceStartDate) order by abs(datediff(day, o.STARTDATE, ServiceStartDate)) asc) as _RNum
				from	[CHSP].[Fact_ServiceHours] sh 
						inner join CHSP.Dim_Branch brnch on brnch.BranchKey = sh.BranchKey
						inner join [Audb_IAL_Procura_Mart].[dbo].[ORDERS] o on sh.ClientID = o.CLIENT_ID and o.DEPT_ID = brnch.BranchAlternateKey
						inner join [Audb_IAL_Procura_Mart].[dbo].[FUNDERS] f on f.FUNDER_ID = o.FUNDER_ID											
				where	convert(date, [ServiceStartDate]) between @IFromDate and @IToDate
				and		SupervisoryHours is not null
				and		(f.CODE like 'CHSP%' or f.CODE like 'NRCP%' or f.code like 'HACC%' or f.code like 'CCSP%')				
		) a
		where	a._RNum = 1

)
select	sh.[ClientID], sh.VisitID as SessionID, sh.[ServiceStartDate] as VisitDate
		,o.ORDER_ID as CaseID, olt.OutletactivityId, isnull(brv.BILLCODE, 42) as ServiceTypeId
		,0 as TotalNumberOfUnidentifiedClients4, 'false' as InterpreterPresent, 'CLIENT' as ParticipationCode
		,[ServiceHours] * 60 as TimeMinutes, [ServiceHours] * br.BILLRATE as FeesCharged, brnch.Entity
into	#SHRaw
from	[CHSP].[Fact_ServiceHours] sh
		inner join CHSP.Dim_Branch brnch on brnch.CostCentre = sh.BranchCodeOriginal
		inner join CHSP.Dim_FundingService fs on fs.ServiceTypeKey = sh.ServiceTypeKey
		inner join Audb_IAL_Procura_Mart.[dbo].VISITS_ALL v on sh.VisitID = v.VISIT_ID
		inner join Audb_IAL_Procura_Mart.[dbo].ORDERS o on v.ORDER_ID = o.ORDER_ID
		inner join Audb_IAL_Procura_Mart.[dbo].BILLRECS br on v.BILLREC_ID = br.BILLREC_ID
		left join #Outlets2 olt on o.FUNDER_ID = olt.FUNDER_ID

		outer apply 
		(
			select	top 1 BILLCODE
			from	Audb_IAL_Procura_Mart.dbo.BILLRECval val
			where	LOOKUPVAL_ID = 'A0000037084'
			and		val.billrec_id = br.billrec_id
		) brv
where	convert(date, [ServiceStartDate]) between @IFromDate and @IToDate
and		SupervisoryHours is null
and		fs.ServiceTypeDesc not in ('Transport', 'Home Mods ($)', 'Meals', 'Case Management')

union all

-- Try and assign an order id to offline hours. Pick the order id that is closest to the offline day (doesn't matter before or after). 
-- This will form the case id in DEX output and contribute to the correct selection of OutletactivityId
-- Try and scrape orders and outlet ids using both original and translated branch codes
select	sh.[ClientID], sh.VisitID as SessionID, sh.[ServiceStartDate] as VisitDate
		,coalesce(ordorig.ORDER_ID, ord.ORDER_ID) as CaseID, coalesce(oltorig.OutletactivityId, olt.OutletactivityId) as OutletactivityId, case when (fs.ServiceType = 'Domestic Assistance') then 42 else 62 end as ServiceTypeId
		,0 as TotalNumberOfUnidentifiedClients4, 'false' as InterpreterPresent, 'CLIENT' as ParticipationCode
		,[ServiceHours] * 60 as TimeMinutes, null as FeesCharged, brnch.Entity
from	[CHSP].[Fact_ServiceHours] sh
		inner join CHSP.Dim_Branch brnch on brnch.CostCentre = sh.BranchCodeOriginal
		inner join CHSP.Dim_FundingService fs on fs.ServiceTypeKey = sh.ServiceTypeKey

		left join c_ordersorig ordorig on ordorig.ClientID = sh.ClientID and ordorig.BranchKey = brnch.BranchKey and convert(DATE, ordorig.ServiceStartDate) = sh.ServiceStartDate
		left join #Outlets2 oltorig on ordorig.FUNDER_ID = oltorig.FUNDER_ID

		left join c_orders ord on ord.ClientID = sh.ClientID and ord.BranchKey = sh.BranchKey and convert(DATE, ord.ServiceStartDate) = sh.ServiceStartDate
		left join #Outlets2 olt on ord.FUNDER_ID = olt.FUNDER_ID

where	convert(date, sh.[ServiceStartDate]) between @IFromDate and @IToDate
and		SupervisoryHours is not null
;


-- =================================================================================================================
-- DEX_Sessions
-- =================================================================================================================
select	ClientID, SessionID, CaseID, VisitDate as SessionDate, ServiceTypeId
		,TotalNumberOfUnidentifiedClients4, FeesCharged, InterpreterPresent, ParticipationCode, TimeMinutes, Entity
into	#session
from	#SHRaw
;


-- =================================================================================================================
-- DEX_Cases
-- =================================================================================================================
select	distinct ClientID, CaseID, OutletactivityId, TotalNumberOfUnidentifiedClients4 as TotalNumberOfUnidentifiedClients, Entity
into	#case
from	#SHRaw
;


-- =================================================================================================================
-- DEX_Disability
-- =================================================================================================================
select	*
into	#disability
from
(
		select	distinct cd.CLIENT_ID as ClientID
				,coalesce(case when cd.CODE IN ('LEARNING', 'PSYCHIATRIC', 'SENSORY', 'PHYSICAL', 'NOTSTATED') then cd.CODE
					when cd.CODE = 'PSYCHIATRI' then 'PSYCHIATRIC'
					when cd.code IN ('Hearing', 'Vision', 'Deafblind') then 'SENSORY' 
					else ''
					end, '') as DisabilityCode
				,Entity
		from	Audb_IAL_Procura_Mart.dbo.CLTDIAG cd
				inner join #SHRaw craw on craw.ClientID = cd.CLIENT_ID
		where	CODETYPE = 'DISABILITY'
) a
where	DisabilityCode <> ''
;


-- =================================================================================================================
-- DEX_Clients
-- =================================================================================================================
select	distinct c.CLIENT_ID as ClientID, 'true' as ConsentToProvideDetails, 'true' as ConsentedForFutureContacts, c.FIRSTNAME as GivenName, c.LasTNAME as FamilyName
		,case when c.LasTNAME is not null then 'false' else '' end as IsUsingPsuedonym, convert(date, BIRTHDATE, 120) as BirthDate
		,case when c.ESTBIRTHDATE = 'T' then 'true' else 'false' end IsBirthDateAnEstimate
		,case when c.GENDER = 'Male' then 'MALE'
			when c.GENDER = 'Female' then 'FEMALE'
			when c.GENDER = 'Intersex' then 'INTERSEX'
			else 'NOTSTATED'
			end as GenderCode		
		,coalesce(CountryOfBirthCode.Code, '0003') as CountryOfBirthCode, coalesce(LanguageSpokenAtHomeCode.lcode, '0002') as LanguageSpokenAtHomeCode
		,case when c.ETHNICITY = 'Aboriginal' then 'ABorIGINAL'
			when c.ETHNICITY = 'Both Aboriginal and TSI' then 'BOTH'
			when c.ETHNICITY = 'Neither Aboriginal nor TSI' then 'NO'
			when c.ETHNICITY = 'Torres Strait Islander' then 'TSI'
			else 'NOTSTATED'
			end as AboriginalorTorresStraitIslanderoriginCode
		,case when c.CURRESTYPE = 'Aboriginal Rental' or c.CURRESTYPE = 'Indigenous Community' then 'COMMUNITY'
			when c.CURRESTYPE = 'Boarding/priv. hotel' then 'BOARDING'	
			when c.CURRESTYPE = 'Crisis Emerg Trans' or c.CURRESTYPE = 'Short-Term Crisis' then 'TRANSITION'
			when c.CURRESTYPE = 'ILU' or c.CURRESTYPE = 'ILU-Retirement Vill' then 'LIVINGUNIT'
			when c.CURRESTYPE = 'InstitutionalSetting' or c.CURRESTYPE = 'Res High' or c.CURRESTYPE = 'Res Low' or c.CURRESTYPE = 'Res Transition' then 'INSTITUTION'
			when c.CURRESTYPE = 'Private-own/purchase' then 'CLIENTOWNED'
			when c.CURRESTYPE = 'Private-priv. rental' then 'PRIVATERENTAL'
			when c.CURRESTYPE = 'Private-pub. rental' then 'PUBLICRENTAL'
			when c.CURRESTYPE = 'Supported Accomm' then 'SUPPORTED'
			when c.CURRESTYPE = 'Temporary Shelter' then 'PUBLICSHELTER'
			else 'NOTSTATED'
			end as AccommodationTypeCode
		,case when DVACardStatusCode.DVAColour = 'Gold' then 'GOLD'
			when DVACardStatusCode.DVAColour = 'Orange' then 'ORANGE'
			when DVACardStatusCode.DVAColour = 'White' then 'WHITE'
			else 'NODVA'
			end as DVACardStatusCode
		,case when HasCarer.CarerType = 'Carer' then 'true' else 'false' end as HasCarer
		,case when (dis.ClientID is not null) then 'true' else 'false' end as HasDisabilities
		,case when c.LIVING_ARR = 'Couple' then 'COUPLE'
			when c.LIVING_ARR = 'Couple With Dep' then 'FAMILY'
			when c.LIVING_ARR = 'Group - Unrelated' then 'GROUP2'
			when c.LIVING_ARR = 'Homeless' then 'HOMELESS'
			when c.LIVING_ARR = 'Lives alone' or c.LIVING_ARR = 'Single Alone' then 'SINGLE'
			when c.LIVING_ARR = 'Lives with family' then 'GROUP'
			when c.LIVING_ARR = 'Sole Parent Dep' then 'SOLEPARENT'
			else 'NOTSTATED'
			end as HouseholdCompositionCode
		,CURR_ADDR1 as AddressLine1, CURR_CITY as Suburb, CURR_PROV as StateCode, CURR_POST as Postcode, CURR_ADDR2 as AddressLine2
		,craw.Entity
into	#client
from	#SHRaw craw
		inner join Audb_IAL_Procura_Mart.dbo.CLIENTS c on craw.ClientID = c.CLIENT_ID

		left join
		(
			select	distinct ClientID
			from	#disability
		) dis
		on dis.ClientID = c.CLIENT_ID

		outer apply 
		(
			select	top 1 DVA.LOOKUP_ID
					,lv.LOOKVALUE
					,lv.DESCR
					,coalesce(lv.AUXVALUE, 'NODVA') as DVAColour
			from	Audb_IAL_Procura_Mart.dbo.CL_REFNOS CLR
					left join Audb_IAL_Procura_Mart.[dbo].[NUMBERS] DVA on clr.NUMBER_ID = dva.number_id
					left join Audb_IAL_Procura_Mart.dbo.LOOKUPVALS lv on dva.LOOKUP_ID = lv.LOOKUP_ID
					AND CLR.NUMVAL = lv.LOOKVALUE
			where	dva.name = 'Card DVA Colour'
			and		CLR.client_id = c.CLIENT_ID
		) DVACardStatusCode

		outer apply 
		(
			select	top 1 INFC.CLIENT_ID
					,'Carer' as CarerType
			from	Audb_IAL_Procura_Mart.dbo.INFConTACT INFC
			where	INFC.CLIENT_ID = c.CLIENT_ID
			and		type LIKE '%Carer%'
		) HasCarer

		outer apply 
		(
			select	coalesce(LV.REFVAL, '0003') as Code
					,LV.LOOKVALUE
			from	Audb_IAL_Procura_Mart.dbo.LOOKUPVALS lv
			where	c.COUNTRYOFBIRTH = lv.LOOKVALUE
			and		lv.LOOKUP_ID = 'CB'
		) CountryOfBirthCode

		outer apply 
		(
			select	COALESCE(LV.REFVAL, '0002') as LCode
					,LV.LOOKVALUE
			from	Audb_IAL_Procura_Mart.dbo.LOOKUPVALS lv --and CLR.NUMVAL = lv.LOOKVALUE
					left join Audb_IAL_Procura_Mart.DBO.LOOKUPS L on l.LOOKUP_ID = lv.LOOKUP_ID and l.NAME = 'Language'
			where	c.PRILANG = lv.LOOKUPVAL_ID
		) LanguageSpokenAtHomeCode
;


-- =================================================================================================================
-- Insert client, session, case and disability into AURLM and AUHCS_ABORIGINAL tables
-- =================================================================================================================
if object_id('dbo.DEX_Clients_AURLM') is not null 
begin
	set		@RenameTable = 'DEX_Clients_AURLM_RenamedOn' + @Timestamp; 
	exec	sp_rename 'dbo.DEX_Clients_AURLM', @RenameTable; 
end

select	IDENTITY(INT, 1, 1) AS ID
		,*
into	dbo.DEX_Clients_AURLM
from	#client
where	Entity = 'AURLM'
;


if object_id('dbo.DEX_Clients_AUHCS_ABORIGINAL') is not null 
begin
	set		@RenameTable = 'DEX_Clients_AUHCS_ABORIGINAL_RenamedOn' + @Timestamp; 
	exec	sp_rename 'dbo.DEX_Clients_AUHCS_ABORIGINAL', @RenameTable; 
end

select	IDENTITY(INT, 1, 1) AS ID
		,*
into	dbo.DEX_Clients_AUHCS_ABORIGINAL
from	#client
where	Entity <> 'AURLM'
;


if object_id('dbo.DEX_Sessions_AURLM') is not null 
begin
	set		@RenameTable = 'DEX_Sessions_AURLM_RenamedOn' + @Timestamp; 
	exec	sp_rename 'dbo.DEX_Sessions_AURLM', @RenameTable; 
end

select	IDENTITY(INT, 1, 1) AS ID
		,*
into	dbo.DEX_Sessions_AURLM
from	#session
where	Entity = 'AURLM'
;


if object_id('dbo.DEX_Sessions_AUHCS_ABORIGINAL') is not null 
begin
	set		@RenameTable = 'DEX_Sessions_AUHCS_ABORIGINAL_RenamedOn' + @Timestamp; 
	exec	sp_rename 'dbo.DEX_Sessions_AUHCS_ABORIGINAL', @RenameTable; 
end

select	IDENTITY(INT, 1, 1) AS ID
		,*
into	dbo.DEX_Sessions_AUHCS_ABORIGINAL
from	#session
where	Entity <> 'AURLM'
;


if object_id('dbo.DEX_Cases_AURLM') is not null 
begin
	set		@RenameTable = 'DEX_Cases_AURLM_RenamedOn' + @Timestamp; 
	exec	sp_rename 'dbo.DEX_Cases_AURLM', @RenameTable; 
end

select	IDENTITY(INT, 1, 1) AS ID
		,*
into	dbo.DEX_Cases_AURLM
from	#case
where	Entity = 'AURLM'
;


if object_id('dbo.DEX_Cases_AUHCS_ABORIGINAL') is not null 
begin
	set		@RenameTable = 'DEX_Cases_AUHCS_ABORIGINAL_RenamedOn' + @Timestamp; 
	exec	sp_rename 'dbo.DEX_Cases_AUHCS_ABORIGINAL', @RenameTable; 
end

select	IDENTITY(INT, 1, 1) AS ID
		,*
into	dbo.DEX_Cases_AUHCS_ABORIGINAL
from	#case
where	Entity <> 'AURLM'
;


if object_id('dbo.DEX_Disability_AURLM') is not null 
begin
	set		@RenameTable = 'DEX_Disability_AURLM_RenamedOn' + @Timestamp; 
	exec	sp_rename 'dbo.DEX_Disability_AURLM', @RenameTable; 
end

select	IDENTITY(INT, 1, 1) AS ID
		,*
into	dbo.DEX_Disability_AURLM
from	#disability
where	Entity = 'AURLM'
;


if object_id('dbo.DEX_Disability_AUHCS_ABORIGINAL') is not null 
begin
	set		@RenameTable = 'DEX_Disability_AUHCS_ABORIGINAL_RenamedOn' + @Timestamp; 
	exec	sp_rename 'dbo.DEX_Disability_AUHCS_ABORIGINAL', @RenameTable; 
end

select	IDENTITY(INT, 1, 1) AS ID
		,*
into	dbo.DEX_Disability_AUHCS_ABORIGINAL
from	#disability
where	Entity <> 'AURLM'
;


-- =================================================================================================================
-- Patch where cases still can't be found with default cases based on client's history
-- =================================================================================================================
;with	c_patch
as
(
		select	*
		from
		(
				select	ClientID, CaseID, OutletactivityId, row_number() over (partition by ClientID order by CaseID) as _RNum
				from	dbo.DEX_Cases_AURLM
				where	CaseID is not null
				and		ClientID in
				(
						select	distinct ClientID
						from	dbo.DEX_Cases_AURLM
						where	CaseID is null
				)	
		) a 
		where	a._RNum = 1	
)
update	t
set		CaseID = p.CaseID, OutletactivityId = p.OutletactivityId
from	dbo.DEX_Cases_AURLM t
		inner join c_patch p on p.ClientID = t.ClientID
where	t.CaseID is null; 


;with	c_patch
as
(
		select	*
		from
		(
				select	ClientID, CaseID, row_number() over (partition by ClientID order by CaseID) as _RNum
				from	dbo.DEX_Sessions_AURLM
				where	CaseID is not null
				and		ClientID in
				(
						select	distinct ClientID
						from	dbo.DEX_Sessions_AURLM
						where	CaseID is null
				)	
		) a 
		where	a._RNum = 1	
)
update	t
set		CaseID = p.CaseID
from	dbo.DEX_Sessions_AURLM t
		inner join c_patch p on p.ClientID = t.ClientID
where	t.CaseID is null; 

;with	c_patch
as
(
		select	*
		from
		(
				select	ClientID, CaseID, OutletactivityId, row_number() over (partition by ClientID order by CaseID) as _RNum
				from	dbo.DEX_Cases_AUHCS_ABORIGINAL
				where	CaseID is not null
				and		ClientID in
				(
						select	distinct ClientID
						from	dbo.DEX_Cases_AUHCS_ABORIGINAL
						where	CaseID is null
				)	
		) a 
		where	a._RNum = 1	
)
update	t
set		CaseID = p.CaseID, OutletactivityId = p.OutletactivityId
from	dbo.DEX_Cases_AUHCS_ABORIGINAL t
		inner join c_patch p on p.ClientID = t.ClientID
where	t.CaseID is null; 


;with	c_patch
as
(
		select	*
		from
		(
				select	ClientID, CaseID, row_number() over (partition by ClientID order by CaseID) as _RNum
				from	dbo.DEX_Sessions_AUHCS_ABORIGINAL
				where	CaseID is not null
				and		ClientID in
				(
						select	distinct ClientID
						from	dbo.DEX_Sessions_AUHCS_ABORIGINAL
						where	CaseID is null
				)	
		) a 
		where	a._RNum = 1	
)
update	t
set		CaseID = p.CaseID
from	dbo.DEX_Sessions_AUHCS_ABORIGINAL t
		inner join c_patch p on p.ClientID = t.ClientID
where	t.CaseID is null; 
