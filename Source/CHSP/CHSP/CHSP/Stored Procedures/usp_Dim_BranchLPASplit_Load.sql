﻿





CREATE procedure [CHSP].[usp_Dim_BranchLPASplit_Load] 
as
-- exec [CHSP].[usp_Dim_BranchLPASplit_Load]
-- =================================================================================================================
--
-- Author:			M.Rama
--
-- Description:		Upkeeps split from a central source
--
-- Purpose:			Various use cases
--					
--
-- Run Frequency:	TBD
--
-- =================
-- Revision History:
-- =================								 			
--
-- Version		Date		Who		Description		
-- ----------------------------------------------------------------------------------------------------------------
--  1.0			03/04/2019	M.Rama		Initial
-- =================================================================================================================


-- =================================================================================================================
-- Turn off messages SQL sends back to the client after each TSQL Statement
-- =================================================================================================================
set nocount on;


-- =================================================================================================================
-- Merge into target
-- =================================================================================================================
merge	[CHSP].[Dim_BranchLPASplit] t
using	[CHSP].[Staging_Dim_BranchLPASplit] r 
		on (t.[BranchCode] = r.[BranchCode])

when	matched then update set
		t.[SplitPercentage] = r.[SplitPercentage]
		,t.[CHSPLPA] = r.[CHSPLPA]

when	not matched then insert
		(
			[BranchCode]
			,[CHSPLPA]
			,[SplitPercentage]								
		)
		values
		(
			r.[BranchCode]
			,r.[CHSPLPA]
			,r.[SplitPercentage]							
		)

when	not matched by source then
		delete
;

