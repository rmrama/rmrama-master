﻿





CREATE procedure [CHSP].[usp_Fact_ActiveClients_Load] 
as
-- exec [CHSP].[usp_Fact_ActiveClients_Load]
-- =================================================================================================================
--
-- Author:			M.Rama
--
-- Description:		Extracts clients data for summary reporting
--
-- Purpose:			Various use cases
--					
--
-- Run Frequency:	TBD
--
-- =================
-- Revision History:
-- =================								 			
--
-- Version		Date		Who		Description		
-- ----------------------------------------------------------------------------------------------------------------
--  1.0			03/04/2019	M.Rama		Initial
-- =================================================================================================================


-- =================================================================================================================
-- Turn off messages SQL sends back to the client after each TSQL Statement
-- =================================================================================================================
set nocount on;


-- =================================================================================================================
-- Localize parameter
-- =================================================================================================================
declare @FloorDate date; 
declare @CeilingDate date; 


-- =================================================================================================================
-- Prepare working tables
-- =================================================================================================================
if object_id('tempdb..#ResultPreBranch') is not null drop table #ResultPreBranch; 
if object_id('tempdb..#ResultPre') is not null drop table #ResultPre; 
if object_id('tempdb..#Result') is not null drop table #Result; 
if object_id('tempdb..#BranchMapATSI') is not null drop table #BranchMapATSI; 
if object_id('tempdb..#BranchMapNonATSI') is not null drop table #BranchMapNonATSI; 
if object_id('tempdb..#MoveLachlantoSoutherHighlands') is not null drop table #MoveLachlantoSoutherHighlands; 
if object_id('tempdb..#BranchBorrow') is not null drop table #BranchBorrow; 


-- =================================================================================================================
-- Take copy of mapping table. Makes joins below easier to read. 
-- =================================================================================================================
select	CostCentreFrom, CostCentreTo
into	#BranchMapNonATSI
from	[CHSP].[Dim_BranchMapping]
where	[ATSIOnly] = 'F'; 

select	CostCentreFrom, CostCentreTo
into	#BranchMapATSI
from	[CHSP].[Dim_BranchMapping]
where	[ATSIOnly] = 'T'; 


-- =================================================================================================================
-- Derive branch surrogate key. 
-- Map certain branch codes to another branch.
-- For Aborigial clients, map to certain branch.  
-- =================================================================================================================
select	VisitID
		,ClientID
		,ClientDOB
		,VisitStartDate
		,VisitEndDate
		,BranchCodeOriginal
		,BranchCode
		,FunderCode
		,FunderID
		,FunderName	
		,br.Entity as BranchEntity
		,coalesce(br.BranchKey, -1) as BranchKey	
		,coalesce(br.[LPAKey], -1) as [CHSPLPAKey]		
		,BillRecDescription
		,OrderDescription
		,[CurrentPostalCode]
		,[ATSIClient]	
into	#ResultPreBranch
from	
(
		select	ac.VisitID
				,ac.ClientID
				,ac.ClientDOB
				,ac.VisitStartDate
				,ac.VisitEndDate
				,ac.BranchCode as BranchCodeOriginal
				,coalesce(case when ac.[ATSIClient] = 'T' then brAtsi.CostCentreTo else brNonAtsi.CostCentreTo end, ac.BranchCode) as BranchCode
				,ac.FunderCode
				,ac.FunderID
				,ac.FunderName					
				,ac.BillRecDescription
				,ac.OrderDescription
				,ac.[CurrentPostalCode]
				,ac.[ATSIClient]		
		from	[CHSP].[Staging_Fact_ActiveClients] ac		
				left join #BranchMapNonATSI brNonAtsi on brNonAtsi.CostCentreFrom = replace(ac.BranchCode, 'D', '')
				left join #BranchMapATSI brAtsi on brAtsi.CostCentreFrom = replace(ac.BranchCode, 'D', '')
) a 
		left join [CHSP].[Dim_Branch] br on br.CostCentre = replace(a.BranchCode, 'D', '')		
;


-- =================================================================================================================
-- Work out service types for RLM and non RLM branches. 
-- =================================================================================================================
select	VisitID
		,ClientID
		,ClientDOB
		,VisitStartDate
		,VisitEndDate
		,BranchCodeOriginal
		,BranchCode
		,pre.FunderCode
		,FunderID
		,FunderName		
		,BranchKey
		,coalesce(fs.ServiceTypeKey, -1) as ServiceTypeKey 
		,[CHSPLPAKey]
		,BillRecDescription
		,OrderDescription
		,fst.ServiceceType as ServiceTypeOriginal
		,coalesce(fsm.[ServiceTypeTo], fst.ServiceceType) as ServiceType
		,pre.[CurrentPostalCode]
		,[ATSIClient]		
into	#ResultPre
from	#ResultPreBranch pre
		left join Map.FunderCode_ServiceType fst on pre.FunderCode = fst.FunderCode
		left join [CHSP].[Dim_FundingServiceTypeMapping] fsm on fsm.[ServiceTypeFrom] = fst.ServiceceType
		left join [CHSP].[Dim_FundingService] fs on fs.[ServiceTypeDesc] =  coalesce(fsm.[ServiceTypeTo], fst.ServiceceType)
where	BranchEntity <> 'AURLM'
and		(
			fst.Funder in ('CHSP', 'CCSP', 'HACC', 'NRCP') 
			or pre.FunderCode like '%CHSP%' 
			or pre.FunderCode like '%HACC%'
			or ServiceType like '%CHSP%'
			or OrderDescription like '%CHSP%'
			or ServiceType like '%CHSP%'
			or pre.FunderCode in ('NRCP-CR-IH', 'NRCP-C-WS', 'BSOUT-HOMI', 'BSOUT-WHH')
			or (pre.FunderCode = 'NRCP-CR-WS' AND ServiceType like 'IH%')
		)

union

select	VisitID
		,ClientID
		,ClientDOB
		,VisitStartDate
		,VisitEndDate
		,BranchCodeOriginal
		,BranchCode
		,FunderCode
		,FunderID
		,FunderName		
		,BranchKey
		,coalesce(fs.ServiceTypeKey, -1) as ServiceTypeKey				
		,[CHSPLPAKey]
		,BillRecDescription
		,OrderDescription
		,ServiceTypeOriginal	
		,coalesce(fsm.[ServiceTypeTo], a.ServiceTypeOriginal) as ServiceType
		,[CurrentPostalCode]
		,[ATSIClient]		
from 
(
		select	VisitID
				,ClientID
				,ClientDOB
				,VisitStartDate
				,VisitEndDate
				,BranchCodeOriginal
				,BranchCode
				,pre.FunderCode
				,FunderID
				,FunderName		
				,BranchKey
				,case 
					when BillRecDescription like '%SS:%' then 'Social Support Individual'
					when (BillRecDescription like '%Personal%' or BillRecDescription like '%PC%' or [OrderDescription] like '%Personal%' or BillRecDescription = 'Primary & Secondary Health Care') then 'Personal Care'
					when (BillRecDescription like '%Domestic%' or BillRecDescription like '%DA:%' or [OrderDescription] like '%Dome%') then 'Domestic Assistance'
					when (BillRecDescription like '%Resp%' or BillRecDescription like '%RC%' or BillRecDescription like '%DR%' or [OrderDescription] like '%Resp%') then 'Flexible Respite'
					when (BillRecDescription like '%Social%' or BillRecDescription like '%SS%' or BillRecDescription = 'Social Support Individual') then 'Social Support Individual'
					when (BillRecDescription = 'Allied Health') then 'Allied Health & Therapy Services'
					else BillRecDescription
					end	as ServiceTypeOriginal	
				,[CHSPLPAKey]
				,BillRecDescription
				,OrderDescription
				,pre.[CurrentPostalCode]
				,[ATSIClient]				
		from	#ResultPreBranch pre
		where	BranchEntity = 'AURLM'
) a 
		left join [CHSP].[Dim_FundingServiceTypeMapping] fsm on fsm.[ServiceTypeFrom] = a.ServiceTypeOriginal
		left join [CHSP].[Dim_FundingService] fs on fs.[ServiceTypeDesc] = 	coalesce(fsm.[ServiceTypeTo], a.ServiceTypeOriginal)
		
where	a.FunderCode like '%CHSP%' 
or		a.FunderCode like '%HACC%'
or		ServiceType like '%CHSP%'
or		OrderDescription like '%CHSP%'
or		ServiceType like '%CHSP%'
or		a.FunderCode in ('NRCP-CR-IH', 'NRCP-C-WS', 'BSOUT-HOMI', 'BSOUT-WHH')
or		(a.FunderCode = 'NRCP-CR-WS' AND ServiceType like 'IH%')
;


-- =================================================================================================================
-- Move Southern Highlands clients back from Lachlan. 
-- Move new clients from Lachlan to Southern Highlands. 
-- =================================================================================================================
select	sh.ClientID, lc.BranchCode as FromBranchCode, sh.BranchCode as ToBranchCode, LPAKey
into	#MoveLachlantoSoutherHighlands
from 
(
		select	distinct ClientID, BranchCode
		from	#ResultPreBranch pre
				inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = pre.BranchKey
		where	BranchCode = '6917'
		and		convert(date, VisitStartDate) <= '2018-12-01'
		union
		select	distinct ClientID, BranchCode
		from	[CHSP].[Fact_ActiveClients] ac 
				inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = ac.BranchKey
		where	BranchCode = '6917'
		and		convert(date, VisitStartDate) <= '2018-12-01'
) sh
		inner join 
(
		select	distinct ClientID, BranchCode, br.LPAKey
		from	#ResultPreBranch pre
				inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = pre.BranchKey
		where	BranchCode = '6869'
		and		convert(date, VisitStartDate) > '2018-12-01'
		union
		select	distinct ClientID, BranchCode, br.LPAKey
		from	[CHSP].[Fact_ActiveClients] ac 
				inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = ac.BranchKey
		where	BranchCode = '6869'
		and		convert(date, VisitStartDate) > '2018-12-01'
) lc	on sh.ClientID = lc.ClientID

union 

select	sh.ClientID, lc.BranchCode as FromBranchCode, sh.BranchCode as ToBranchCode, LPAKey
from 
(
		select	distinct ClientID, BranchCode
		from	#ResultPreBranch pre
				inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = pre.BranchKey
		where	BranchCode = '6982'
		and		convert(date, VisitStartDate) <= '2018-12-01'
		union
		select	distinct ClientID, BranchCode
		from	[CHSP].[Fact_ActiveClients] ac 
				inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = ac.BranchKey
		where	BranchCode = '6982'
		and		convert(date, VisitStartDate) <= '2018-12-01'
) sh
		inner join 
(
		select	distinct ClientID, BranchCode, br.LPAKey
		from	#ResultPreBranch pre
				inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = pre.BranchKey
		where	BranchCode = '6973'
		and		convert(date, VisitStartDate) > '2018-12-01'
		union
		select	distinct ClientID, BranchCode, br.LPAKey
		from	[CHSP].[Fact_ActiveClients] ac 
				inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = ac.BranchKey
		where	BranchCode = '6973'
		and		convert(date, VisitStartDate) > '2018-12-01'
) lc	on sh.ClientID = lc.ClientID

union

select	distinct ClientID, BranchCode as FromBranchCode, '6917' as ToBranchCode, br.LPAKey
from	#ResultPreBranch pre
		inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = pre.BranchKey
where	BranchCode = '6869'
and		[CurrentPostalCode] in ('2594','2586' ,'2587')
and		convert(date, VisitStartDate) > '2018-12-01'

union 

select	distinct ClientID, BranchCode as FromBranchCode, '6982' as ToBranchCode, br.LPAKey
from	#ResultPreBranch pre
		inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = pre.BranchKey
where	BranchCode = '6973'
and		[CurrentPostalCode] in ('2594','2586' ,'2587')
and		convert(date, VisitStartDate) > '2018-12-01'
;


-- =================================================================================================================
-- Blow the row up to reporting month. Before that, absorb #MoveLachlantoSoutherHighlands
-- In the case when a visit starts in one month and ends in the next month, the client is considered active in both months.
-- Because of the above, when a visit spans across days within a month, this produces duplicates. Distict solves this problem. 
-- =================================================================================================================
select	distinct VisitID
		,pre.ClientID
		,ClientDOB
		,VisitStartDate
		,VisitEndDate
		,BranchCodeOriginal
		,coalesce(m.ToBranchCode, pre.BranchCode) as BranchCode
		,FunderCode
		,FunderID
		,FunderName		
		,BranchKey
		,ServiceTypeKey				
		,coalesce(m.LPAKey, [CHSPLPAKey]) as [CHSPLPAKey]
		,BillRecDescription
		,OrderDescription
		,ServiceTypeOriginal	
		,ServiceType
		,convert(date, dd.[FirstDayOfMonth]) as ReportingMonth	
		,pre.[CurrentPostalCode]	
		,case when ([ATSIClient] = 'T') then convert(bit, 1) else convert(bit, 0) end as [ATSIClient]
into	#Result
from	#ResultPre pre
		left join #MoveLachlantoSoutherHighlands m on m.ClientID = pre.ClientID and m.FromBranchCode = pre.BranchCode and convert(date, VisitStartDate) > '2018-12-01'
		inner join [CHSP].[Dim_Date] dd on 1 = 1 and dd.[FullDateAlternateKey] between convert(date, VisitStartDate) and convert(date, VisitEndDate)		
;


-- =================================================================================================================
-- Derive branch borrow visits. Keep the visit of the delivery branch and remove the visit from the owning branch. 
-- =================================================================================================================
select	distinct OwningVisitID
into	#BranchBorrow
from	#Result r 
		inner join [RDS].[BranchBorrowVisits] bb on r.VisitID = bb.DeliveryVisitId
;

delete	#Result
from	#Result r
		inner join #BranchBorrow bb on bb.OwningVisitID = r.VisitID
;


-- =================================================================================================================
-- Derive floor and ceiling dates. Will be used to delete record in current FY that did not come into staging
-- but exists in Fact. Happens when status in source is changed. 
-- =================================================================================================================
select	@FloorDate = min(VisitStartDate), @CeilingDate = max(VisitStartDate)
from	#Result
;


-- =================================================================================================================
-- Whole bunch of LPA overrides for certain service types in certain branches. 
-- Need to find a way to implement this as a mapping table. It's weird because only the LPA is changed
-- instead of mapping to a branch inside the LPA. 
-- =================================================================================================================
update	#Result
set		CHSPLPAKey = (select CHSPLPAKey from CHSP.Dim_LPA where	CHSPLPADesc = 'Orana Far West')
from	#Result r
		inner join CHSP.Dim_Branch b on b.BranchKey = r.BranchKey		
where	FunderName like '%Orana%'
and		(b.Department like '%Calare%' or b.Department like '%Lachlan%')
;


-- =================================================================================================================
-- Merge into target
-- =================================================================================================================
merge	[CHSP].[Fact_ActiveClients] t
using	#Result r 
		on (t.[VisitID] = r.[VisitID] and t.[ClientID] = r.[ClientID] and t.[ReportingMonth] = r.[ReportingMonth])

when	matched then update set
		t.[ClientDOB] = r.[ClientDOB]
		,t.[VisitStartDate] = r.[VisitStartDate]
		,t.[VisitEndDate] = r.[VisitEndDate]
		,t.[FunderID] = r.[FunderID]
		,t.[FunderCode] = r.[FunderCode]
		,t.[FunderName] = r.[FunderName]
		,t.[BranchCodeOriginal] = r.[BranchCodeOriginal]
		,t.[BranchCode] = r.[BranchCode]
		,t.[ServiceTypeOriginal] = r.[ServiceTypeOriginal]
		,t.[ServiceType] = r.[ServiceType]
		,t.[ReportingMonth] = r.[ReportingMonth]
		,t.[BranchKey] = r.[BranchKey]
		,t.[ServiceTypeKey] = r.[ServiceTypeKey]
		,t.[CHSPLPAKey] = r.[CHSPLPAKey]		
		,t.[BillRecDescription] = r.[BillRecDescription]	
		,t.[OrderDescription] = r.[OrderDescription]	
		,t.[CurrentPostalCode] = r.[CurrentPostalCode]	
		,t.[ATSIClient] = r.[ATSIClient]

when	not matched then insert
		(
			[VisitID]
			,[ClientID]
			,[ClientDOB]
			,[VisitStartDate]
			,[VisitEndDate]
			,[FunderID]
			,[FunderCode]
			,[FunderName]
			,[BranchCodeOriginal]
			,[BranchCode]
			,[ServiceTypeOriginal]
			,[ServiceType]
			,[ReportingMonth]
			,[BranchKey]
			,[ServiceTypeKey]
			,[CHSPLPAKey]
			,[BillRecDescription]
			,[OrderDescription]
			,[CurrentPostalCode]
			,[ATSIClient]
		)
		values
		(
			r.[VisitID]
			,r.[ClientID]
			,r.[ClientDOB]
			,r.[VisitStartDate]
			,r.[VisitEndDate]
			,r.[FunderID]
			,r.[FunderCode]
			,r.[FunderName]
			,r.[BranchCodeOriginal]
			,r.[BranchCode]
			,r.[ServiceTypeOriginal]
			,r.[ServiceType]
			,r.[ReportingMonth]
			,r.[BranchKey]
			,r.[ServiceTypeKey]
			,r.[CHSPLPAKey]
			,r.[BillRecDescription]
			,r.[OrderDescription]		
			,r.[CurrentPostalCode]	
			,r.[ATSIClient]
		)

when	not matched by source and convert(date, t.[VisitStartDate]) between @FloorDate and @CeilingDate then
		delete
;

