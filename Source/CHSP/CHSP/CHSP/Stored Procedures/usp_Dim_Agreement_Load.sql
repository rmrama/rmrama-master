﻿




CREATE procedure [CHSP].[usp_Dim_Agreement_Load] 
as
-- exec [CHSP].[usp_Dim_Agreement_Load]
-- =================================================================================================================
--
-- Author:			M.Rama
--
-- Description:		Upkeeps agreements from a central source
--
-- Purpose:			Various use cases
--					
--
-- Run Frequency:	TBD
--
-- =================
-- Revision History:
-- =================								 			
--
-- Version		Date		Who		Description		
-- ----------------------------------------------------------------------------------------------------------------
--  1.0			03/04/2019	M.Rama		Initial
-- =================================================================================================================


-- =================================================================================================================
-- Turn off messages SQL sends back to the client after each TSQL Statement
-- =================================================================================================================
set nocount on;


-- =================================================================================================================
-- Merge into target
-- =================================================================================================================
merge	[CHSP].[Dim_Agreement] t
using	[CHSP].[Staging_Dim_Agreement] r 
		on (t.[CHSPAgreementType] = r.[CHSPAgreementType])

when	matched then update set
		t.[CHSPAgreementTypeDesc] = r.[CHSPAgreementTypeDesc]
		,t.[DisplayOrder] = r.[DisplayOrder]
		,t.[CHSPAgreementTypeGroup] = r.[CHSPAgreementTypeGroup]

when	not matched then insert
		(
			[CHSPAgreementType]
			,[CHSPAgreementTypeDesc]
			,[DisplayOrder]			
			,[CHSPAgreementTypeGroup]			
		)
		values
		(
			r.[CHSPAgreementType]
			,r.[CHSPAgreementTypeDesc]
			,r.[DisplayOrder]			
			,r.[CHSPAgreementTypeGroup]			
		)
;

