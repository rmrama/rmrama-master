﻿

CREATE procedure [CHSP].[usp_ActiveClient_Output] 
as
-- exec [CHSP].[usp_ActiveClient_Output]
-- =================================================================================================================
--
-- Author:			M.Rama
--
-- Description:		Outputs combined data
--
-- Purpose:			For dashboards. Beats views anyday as views are not giving parallel plans with joined with DimDate
--					
--
-- Run Frequency:	TBD
--
-- =================
-- Revision History:
-- =================								 			
--
-- Version		Date		Who		Description		
-- ----------------------------------------------------------------------------------------------------------------
--  1.0			03/04/2019	M.Rama		Initial
-- =================================================================================================================


-- =================================================================================================================
-- Turn off messages SQL sends back to the client after each TSQL Statement
-- =================================================================================================================
set nocount on;


-- =================================================================================================================
-- Localize parameter
-- =================================================================================================================
declare	@MinFullDateAlternateKey date; 
declare	@MaxFullDateAlternateKey date; 
declare	@MaxFullDateAlternateKey2 date; 


-- =================================================================================================================
-- Determine floor and ceiling of current financial year. This fixed the parallel query plan problem.
-- =================================================================================================================
select	@MinFullDateAlternateKey = convert(date, min([FullDateAlternateKey]))
from	[CHSP].[Dim_Date] 
where	convert(int, FinancialYear) = (select FinancialYear from [CHSP].[Dim_Date] where FullDateAlternateKey = convert(date, getdate())); 

select	@MaxFullDateAlternateKey = convert(date, max([FullDateAlternateKey]))
from	[CHSP].[Dim_Date] 
where	convert(int, FinancialYear) = (select FinancialYear from [CHSP].[Dim_Date] where FullDateAlternateKey = convert(date, getdate())); 

select	@MaxFullDateAlternateKey2 = dateadd(day, -1 * (day(_Input)), _Input)
from	
(
		select	dateadd(month, 3, convert(date, getdate())) as _Input
) a

set		@MaxFullDateAlternateKey = case when (@MaxFullDateAlternateKey2 <= @MaxFullDateAlternateKey) then @MaxFullDateAlternateKey2 else @MaxFullDateAlternateKey end;  


-- =================================================================================================================
-- Output big table
-- =================================================================================================================
select	distinct
		br.CostCentre as [Branch Code]
		,br.[Department] as [Branch Name]
		,replace(br.Entity, 'AURLM', 'AUHCS') as [Branch Entity]
		,br.State
		--,case 
		--	when (ac.[FunderName] like '%Orana%' and br.Department like '%Calare%') then 'Orana Far West'
		--	when (ac.[FunderName] like '%Orana%' and br.Department like '%LachLan%') then 'Orana Far West'
		--	else lpa.CHSPLPADesc
		--	end as LPA
		--,case when ((br.Department like '%Calare%' or br.Department like '%Lachlan%') and FunderName like '%Orana%') then 'Orana Far West' else lpa.CHSPLPADesc end as LPA
		,lpa.CHSPLPADesc as LPA
		,fs.[ServiceTypeDesc] as [Service Type]
		,convert(date, VisitStartDate) as [Service Date]
		,[ClientID] as [Client ID]
from	[CHSP].[Fact_ActiveClients] ac
		inner join [CHSP].[Dim_Branch] br on br.BranchKey = ac.BranchKey
		inner join [CHSP].[Dim_LPA] lpa on lpa.CHSPLPAKey = ac.CHSPLPAKey
		inner join [CHSP].[Dim_FundingService] fs on fs.ServiceTypeKey = ac.ServiceTypeKey		
where	[ReportingMonth] between @MinFullDateAlternateKey and @MaxFullDateAlternateKey	
and		fs.[ToDashboard] = 1
;
