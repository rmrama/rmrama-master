﻿


CREATE procedure [CHSP].[usp_ServiceData_Output] 
as
-- exec [CHSP].[usp_ServiceData_Output]
-- =================================================================================================================
--
-- Author:			M.Rama
--
-- Description:		Outputs combined data
--
-- Purpose:			For dashboards. Beats views anyday as views are not giving parallel plans with joined with DimDate
--					
--
-- Run Frequency:	TBD
--
-- =================
-- Revision History:
-- =================								 			
--
-- Version		Date		Who		Description		
-- ----------------------------------------------------------------------------------------------------------------
--  1.0			03/04/2019	M.Rama		Initial
-- =================================================================================================================


-- =================================================================================================================
-- Turn off messages SQL sends back to the client after each TSQL Statement
-- =================================================================================================================
set nocount on;


-- =================================================================================================================
-- Localize parameter
-- =================================================================================================================
declare	@MinFullDateAlternateKey date; 
declare	@MaxFullDateAlternateKey date; 
declare	@MaxFullDateAlternateKey2 date; 


-- =================================================================================================================
-- Prepare working tables
-- =================================================================================================================
if object_id('tempdb..#ServiceHours') is not null drop table #ServiceHours; 
if object_id('tempdb..#Funding') is not null drop table #Funding; 
--if object_id('tempdb..#StockControl') is not null drop table #StockControl; 


-- =================================================================================================================
-- Determine floor and ceiling of current financial year. This fixed the parallel query plan problem.
-- =================================================================================================================
select	@MinFullDateAlternateKey = convert(date, min([FullDateAlternateKey]))
from	[CHSP].[Dim_Date] 
where	convert(int, FinancialYear) = (select FinancialYear from [CHSP].[Dim_Date] where FullDateAlternateKey = convert(date, getdate())); 

select	@MaxFullDateAlternateKey = convert(date, max([FullDateAlternateKey]))
from	[CHSP].[Dim_Date] 
where	convert(int, FinancialYear) = (select FinancialYear from [CHSP].[Dim_Date] where FullDateAlternateKey = convert(date, getdate())); 

select	@MaxFullDateAlternateKey2 = dateadd(day, -1 * (day(_Input)), _Input)
from	
(
		select	dateadd(month, 3, convert(date, getdate())) as _Input
) a

set		@MaxFullDateAlternateKey = case when (@MaxFullDateAlternateKey2 <= @MaxFullDateAlternateKey) then @MaxFullDateAlternateKey2 else @MaxFullDateAlternateKey end;  


-- =================================================================================================================
-- Prepare service hours data
-- =================================================================================================================
select	br.CostCentre as BranchCode
		,br.[Department] as BranchName
		,replace(br.Entity, 'AURLM', 'AUHCS') as BranchEntity
		,br.State		
		--,case 
		--	when (sh.[FunderName] like '%Orana%' and br.Department like '%Calare%') then 'Orana Far West'
		--	when (sh.[FunderName] like '%Orana%' and br.Department like '%LachLan%') then 'Orana Far West'
		--	else lpa.CHSPLPADesc
		--	end as LPA
		--,case when ((br.Department like '%Calare%' or br.Department like '%Lachlan%') and FunderName like '%Orana%') then 'Orana Far West' else lpa.CHSPLPADesc end as LPA
		,lpa.CHSPLPADesc as LPA
		,fs.[ServiceTypeDesc] as ServiceType
		,convert(date, sh.[ServiceStartDate]) as ServiceDate
		,sum([ServiceHoursRaw]) + sum(coalesce([SupervisoryHours], 0)) as [ServiceHoursRaw]
		,sum(ServiceHours3060) + sum(coalesce([SupervisoryHours], 0)) as [ServiceHours3060]
		,sum([ServiceHours]) + sum(coalesce([SupervisoryHours], 0)) as [ServiceHours]		
		,count(distinct sh.ClientID) as ServicedClients		
into	#ServiceHours
from	[CHSP].[Fact_ServiceHours] sh
		inner join [CHSP].[Dim_Branch] br on br.BranchKey = sh.BranchKey
		inner join [CHSP].[Dim_LPA] lpa on lpa.CHSPLPAKey = sh.CHSPLPAKey
		inner join [CHSP].[Dim_FundingService] fs on fs.ServiceTypeKey = sh.ServiceTypeKey
where	convert(date, sh.[ServiceStartDate]) between @MinFullDateAlternateKey and @MaxFullDateAlternateKey	
and		fs.[ToDashboard] = 1
group by
		br.CostCentre
		,br.[Department]
		,br.Entity
		,br.State		
		--,case 
		--	when (sh.[FunderName] like '%Orana%' and br.Department like '%Calare%') then 'Orana Far West'
		--	when (sh.[FunderName] like '%Orana%' and br.Department like '%LachLan%') then 'Orana Far West'
		--	else lpa.CHSPLPADesc
		--	end
		--,case when ((br.Department like '%Calare%' or br.Department like '%Lachlan%') and FunderName like '%Orana%') then 'Orana Far West' else lpa.CHSPLPADesc end
		,lpa.CHSPLPADesc
		,fs.[ServiceTypeDesc]
		,convert(date, sh.[ServiceStartDate])
;


-- =================================================================================================================
-- Prepare funding data
-- =================================================================================================================
select	br.CostCentre as BranchCode
		,br.[Department] as BranchName
		,replace(br.Entity, 'AURLM', 'AUHCS') as BranchEntity		
		,br.State
		,lpa.CHSPLPADesc as LPA
		,fs.[ServiceTypeDesc] as ServiceType
		,convert(date, convert(varchar, [FundingDateKey]), 112) as ServiceDate
		,sum([Funded_Hours]) as [FundedHours]		
into	#Funding
from	[CHSP].[Fact_FundingDaily] fd
		inner join [CHSP].[Dim_Branch] br on br.BranchKey = fd.BranchKey
		inner join [CHSP].[Dim_LPA] lpa on lpa.CHSPLPAKey = fd.CHSPLPAKey
		inner join [CHSP].[Dim_FundingService] fs on fs.ServiceTypeKey = fd.ServiceTypeKey	
where	convert(date, convert(varchar, [FundingDateKey]), 112) between @MinFullDateAlternateKey and @MaxFullDateAlternateKey	
and		fs.[ToDashboard] = 1
group by
		br.CostCentre
		,br.[Department]
		,br.Entity		
		,br.State
		,lpa.CHSPLPADesc
		,fs.[ServiceTypeDesc]
		,convert(date, convert(varchar, [FundingDateKey]), 112)
;


-- =================================================================================================================
-- Output big table
-- First makes sure that service hours are brought in only when the LPA that a branch is acquitting to has funding
-- for the service types that the branch is recording hours for. 
-- After that, join with funding itself. Make funding as the driver, defaulting service hours to 0 when a match is 
-- not found. 
-- =================================================================================================================
with	c_f
as
(
		select  BranchEntity, LPA, ServiceType, sum([FundedHours]) as [FundedHours]
		from	#Funding
		group by
				BranchEntity, LPA, ServiceType 
		having	sum([FundedHours]) > 0
)
,		c_sh
as
(
		select	*
		from	#ServiceHours sh 
		where	exists 
		(
				select	null 
				from	c_f f 
				where	f.LPA = sh.LPA
				and		f.BranchEntity = sh.BranchEntity
				and		f.ServiceType = sh.ServiceType
		)
)
select	coalesce(sh.BranchCode, f.BranchCode) as [Branch Code]
		,coalesce(sh.[BranchName], f.[BranchName]) as [Branch Name]
		,coalesce(sh.[BranchEntity], f.[BranchEntity]) as [Branch Entity]
		,coalesce(sh.[State], f.[State]) as [State]
		,coalesce(sh.[LPA], f.[LPA]) as [LPA]
		,coalesce(sh.[ServiceType], f.[ServiceType]) as [Service Type]
		,coalesce(sh.ServiceDate, f.ServiceDate) as [Service Date]
		,coalesce(f.FundedHours, 0) as [Funded Hours]	
		,coalesce(f.FundedHours, 0) * case when (coalesce(sh.[BranchEntity], f.[BranchEntity]) = 'AUHCS') then 0.84 else 0.7 end as [Target Output Hours]
		,coalesce(sh.ServiceHours, 0) as [Service Hours]		
		,coalesce(sh.ServiceHoursRaw, 0) as [Service Hours Raw]
		,coalesce(sh.[ServiceHours3060], 0) as [Service Hours 3060]
		,coalesce(sh.[ServicedClients], 0) as [Serviced Clients]

		,at.[YellowRangeMin1] as [Yellow Range Min 1]
		,at.[YellowRangeMax1] as [Yellow Range Max 1]
		,at.[YellowRangeMin2] as [Yellow Range Min 2]
		,at.[YellowRangeMax2] as [Yellow Range Max 2]
		,at.[GreenRangeMin] as [Green Range Min]
		,at.[GreenRangeMax] as [Green Range Max]
		,at.[RedRangeMin1] as [Red Range Min 1]
		,at.[RedRangeMax1] as [Red Range Max 1]
		,at.[RedRangeMin2] as [Red Range Min 2]
		,at.[RedRangeMax2] as [Red Range Max 2]
		,at.[Minimum] as [Band Minimum]
		,at.[Maximum] as [Band Maximum]
		,at.[TargetRange] as [Target Range]
from	#Funding f
		--full outer join #StockControl sc on sh.BranchCode = sc.BranchCode and sh.LPA = sc.LPA and sh.ServiceDate = sc.ServiceDate and sh.ServiceType = sc.ServiceType
		full outer join c_sh sh on sh.BranchCode = f.BranchCode and sh.ServiceDate = f.ServiceDate and sh.ServiceType = f.ServiceType and sh.LPA = f.LPA 
		inner join [CHSP].[Fact_AgreementTargets] at on at.CHSPAgreement = coalesce(sh.[BranchEntity], f.[BranchEntity])
;

