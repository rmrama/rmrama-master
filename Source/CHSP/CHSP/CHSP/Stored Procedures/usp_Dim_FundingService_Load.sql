﻿




CREATE procedure [CHSP].[usp_Dim_FundingService_Load] 
as
-- exec [CHSP].[usp_Dim_FundingService_Load]
-- =================================================================================================================
--
-- Author:			M.Rama
--
-- Description:		Upkeeps funding services from a central source
--
-- Purpose:			Various use cases
--					
--
-- Run Frequency:	TBD
--
-- =================
-- Revision History:
-- =================								 			
--
-- Version		Date		Who		Description		
-- ----------------------------------------------------------------------------------------------------------------
--  1.0			03/04/2019	M.Rama		Initial
-- =================================================================================================================


-- =================================================================================================================
-- Turn off messages SQL sends back to the client after each TSQL Statement
-- =================================================================================================================
set nocount on;


-- =================================================================================================================
-- Merge into target
-- =================================================================================================================
merge	[CHSP].[Dim_FundingService] t
using	[CHSP].[Staging_Dim_FundingService] r 
		on (t.[ServiceType] = r.[ServiceType])

when	matched then update set
		t.[ServiceTypeDesc] = r.[ServiceTypeDesc]
		,t.[DisplayOrder] = r.[DisplayOrder]
		,t.[ServiceTypeGroup] = r.[ServiceTypeGroup]

when	not matched then insert
		(
			[ServiceType]
			,[ServiceTypeDesc]
			,[DisplayOrder]			
			,[ServiceTypeGroup]			
		)
		values
		(
			r.[ServiceType]
			,r.[ServiceTypeDesc]
			,r.[DisplayOrder]			
			,r.[ServiceTypeGroup]			
		)
;

