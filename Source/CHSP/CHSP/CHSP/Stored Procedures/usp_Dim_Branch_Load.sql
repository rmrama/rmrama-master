﻿




CREATE procedure [CHSP].[usp_Dim_Branch_Load] 
as
-- exec [CHSP].[usp_Dim_Branch_Load]
-- =================================================================================================================
--
-- Author:			M.Rama
--
-- Description:		Upkeeps branches from a central source
--
-- Purpose:			Various use cases
--					
--
-- Run Frequency:	TBD
--
-- =================
-- Revision History:
-- =================								 			
--
-- Version		Date		Who		Description		
-- ----------------------------------------------------------------------------------------------------------------
--  1.0			03/04/2019	M.Rama		Initial
-- =================================================================================================================


-- =================================================================================================================
-- Turn off messages SQL sends back to the client after each TSQL Statement
-- =================================================================================================================
set nocount on;


-- =================================================================================================================
-- Prepare working tables
-- =================================================================================================================
if object_id('tempdb..#Result') is not null drop table #Result; 


-- =================================================================================================================
-- Derive LPA key
-- =================================================================================================================
select	b.[BranchAlternateKey]
		,b.[Department]
		,mbr.[Region]
		,coalesce(mbr.[Department], b.[Department]) as [Branch]
		,replace(b.[CostCentre], 'D', '') as [CostCentre]
		,b.[Distribution]
		,case when (l.CHSPLPADesc = 'Aboriginal') then l.CHSPLPADesc else b.[Entity] end as Entity
		,b.[State]
		,b.[ATSI]
		,b.[DisplayOrder]
		,coalesce(l.CHSPLPAKey, -1) as [LPAKey]
into	#Result
from	[CHSP].[Staging_Dim_Branch] b
		left join [Map].[BranchesRegion] mbr on b.BranchAlternateKey = mbr.[BranchAlternateKey]
		left join [CHSP].[Dim_BranchLPASplit] spl on replace(b.[CostCentre], 'D', '') = spl.[BranchCode]
		left join [CHSP].[Dim_LPA] l on l.[CHSPLPA] = coalesce(spl.[CHSPLPA], mbr.[LPA])				
;


-- =================================================================================================================
-- Merge into target
-- =================================================================================================================
merge	[CHSP].[Dim_Branch] t
using	#Result r 
		on (t.[CostCentre] = r.[CostCentre])

when	matched then update set
		t.[BranchAlternateKey] = r.[BranchAlternateKey]
		,t.[LPAKey] = r.[LPAKey]
		,t.[Department] = r.[Department]
		,t.[Region] = r.[Region]
		,t.[Branch] = r.[Branch]
		,t.[CostCentre] = r.[CostCentre]
		,t.[Distribution] = r.[Distribution]
		,t.[Entity] = r.[Entity]
		,t.[State] = r.[State]
		,t.[ATSI] = r.[ATSI]
		,t.[DisplayOrder] = r.[DisplayOrder]

when	not matched then insert
		(
			[BranchAlternateKey]
			,[LPAKey]
			,[Department]
			,[Region]
			,[Branch]
			,[CostCentre]
			,[Distribution]
			,[Entity]
			,[State]
			,[ATSI]
			,[DisplayOrder]		
		)
		values
		(
			r.[BranchAlternateKey]
			,r.[LPAKey]
			,r.[Department]
			,r.[Region]
			,r.[Branch]
			,r.[CostCentre]
			,r.[Distribution]
			,r.[Entity]
			,r.[State]
			,r.[ATSI]
			,r.[DisplayOrder]				
		)
;

