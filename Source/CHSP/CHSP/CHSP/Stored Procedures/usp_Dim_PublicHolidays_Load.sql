﻿



CREATE procedure [CHSP].[usp_Dim_PublicHolidays_Load] 
as
-- exec [CHSP].[usp_Dim_PublicHolidays_Load]
-- =================================================================================================================
--
-- Author:			M.Rama
--
-- Description:		Upkeeps public holidays from a central source
--
-- Purpose:			Influences funding on working and non working days
--					
--
-- Run Frequency:	TBD
--
-- =================
-- Revision History:
-- =================								 			
--
-- Version		Date		Who		Description		
-- ----------------------------------------------------------------------------------------------------------------
--  1.0			03/04/2019	M.Rama		Initial
-- =================================================================================================================


-- =================================================================================================================
-- Turn off messages SQL sends back to the client after each TSQL Statement
-- =================================================================================================================
set nocount on;


-- =================================================================================================================
-- Merge into target
-- =================================================================================================================
merge	[CHSP].[Dim_PublicHolidays] t
using	[CHSP].[Staging_Dim_PublicHolidays] r 
		on (t.[DateKey] = r.[DateKey] and t.[HolidayName] = r.[HolidayName])

when	matched then update set
		t.[FullDateAlternateKey] = r.[FullDateAlternateKey]
		,t.[ApplicableTo] = r.[ApplicableTo]
		,t.[IsApplicableTo_NT] = r.[IsApplicableTo_NT]			
		,t.[IsApplicableTo_WA] = r.[IsApplicableTo_WA]	
		,t.[IsApplicableTo_VIC] = r.[IsApplicableTo_VIC]	
		,t.[IsApplicableTo_TAS] = r.[IsApplicableTo_TAS]	
		,t.[IsApplicableTo_NSW] = r.[IsApplicableTo_NSW]
		,t.[IsApplicableTo_SA] = r.[IsApplicableTo_SA]		
		,t.[IsApplicableTo_ACT] = r.[IsApplicableTo_ACT]		
		,t.[IsApplicableTo_QLD] = r.[IsApplicableTo_QLD]		

when	not matched then insert
		(
			[DateKey]
			,[FullDateAlternateKey]
			,[HolidayName]			
			,[ApplicableTo]
			,[IsApplicableTo_NT]			
			,[IsApplicableTo_WA]			
			,[IsApplicableTo_VIC]		
			,[IsApplicableTo_TAS]
			,[IsApplicableTo_NSW]
			,[IsApplicableTo_SA]
			,[IsApplicableTo_ACT]
			,[IsApplicableTo_QLD]
		)
		values
		(
			r.[DateKey]
			,r.[FullDateAlternateKey]
			,r.[HolidayName]			
			,r.[ApplicableTo]
			,r.[IsApplicableTo_NT]			
			,r.[IsApplicableTo_WA]			
			,r.[IsApplicableTo_VIC]			
			,r.[IsApplicableTo_TAS]
			,r.[IsApplicableTo_NSW]
			,r.[IsApplicableTo_SA]
			,r.[IsApplicableTo_ACT]
			,r.[IsApplicableTo_QLD]
		)
;

