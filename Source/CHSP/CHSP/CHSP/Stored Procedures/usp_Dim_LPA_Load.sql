﻿




CREATE procedure [CHSP].[usp_Dim_LPA_Load] 
as
-- exec [CHSP].[usp_Dim_LPA_Load]
-- =================================================================================================================
--
-- Author:			M.Rama
--
-- Description:		Upkeeps LPA from a central source
--
-- Purpose:			Various use cases
--					
--
-- Run Frequency:	TBD
--
-- =================
-- Revision History:
-- =================								 			
--
-- Version		Date		Who		Description		
-- ----------------------------------------------------------------------------------------------------------------
--  1.0			03/04/2019	M.Rama		Initial
-- =================================================================================================================


-- =================================================================================================================
-- Turn off messages SQL sends back to the client after each TSQL Statement
-- =================================================================================================================
set nocount on;


-- =================================================================================================================
-- Merge into target
-- =================================================================================================================
merge	[CHSP].[Dim_LPA] t
using	[CHSP].[Staging_Dim_LPA] r 
		on (t.[CHSPLPA] = r.[CHSPLPA])

when	matched then update set
		t.[CHSPLPADesc] = r.[CHSPLPADesc]
		,t.[DisplayOrder] = r.[DisplayOrder]
		,t.[CHSPAgreement] = r.[CHSPAgreement]

when	not matched then insert
		(
			[CHSPLPA]
			,[CHSPLPADesc]
			,[DisplayOrder]			
			,[CHSPAgreement]			
		)
		values
		(
			r.[CHSPLPA]
			,r.[CHSPLPADesc]
			,r.[DisplayOrder]			
			,r.[CHSPAgreement]			
		)
;

