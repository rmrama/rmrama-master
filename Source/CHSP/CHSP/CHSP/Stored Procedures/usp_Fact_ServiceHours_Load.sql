﻿





CREATE procedure [CHSP].[usp_Fact_ServiceHours_Load] 
as
-- exec [CHSP].[usp_Fact_ServiceHours_Load]
-- =================================================================================================================
--
-- Author:			M.Rama
--
-- Description:		Extracts service hours summary reporting
--
-- Purpose:			Various use cases
--					
--
-- Run Frequency:	TBD
--
-- =================
-- Revision History:
-- =================								 			
--
-- Version		Date		Who		Description		
-- ----------------------------------------------------------------------------------------------------------------
--  1.0			03/04/2019	M.Rama		Initial
-- =================================================================================================================


-- =================================================================================================================
-- Turn off messages SQL sends back to the client after each TSQL Statement
-- =================================================================================================================
set nocount on;


-- =================================================================================================================
-- Localize parameter
-- =================================================================================================================
declare @FloorDate date; 
declare @CeilingDate date; 


-- =================================================================================================================
-- Prepare working tables
-- =================================================================================================================
if object_id('tempdb..#ResultPreBranch') is not null drop table #ResultPreBranch; 
if object_id('tempdb..#ResultPre') is not null drop table #ResultPre; 
if object_id('tempdb..#ResultPreSupervisory') is not null drop table #ResultPreSupervisory; 
if object_id('tempdb..#Result') is not null drop table #Result; 
if object_id('tempdb..#BranchMapATSI') is not null drop table #BranchMapATSI; 
if object_id('tempdb..#BranchMapNonATSI') is not null drop table #BranchMapNonATSI; 
if object_id('tempdb..#MoveLachlantoSoutherHighlands') is not null drop table #MoveLachlantoSoutherHighlands; 
if object_id('tempdb..#B2BLessThan1HourLastVisit') is not null drop table #B2BLessThan1HourLastVisit;
if object_id('tempdb..#B2BLessThan1Hour') is not null drop table #B2BLessThan1Hour; 
if object_id('tempdb..#BranchBorrow') is not null drop table #BranchBorrow; 
if object_id('tempdb..#SupervisoryRaw') is not null drop table #SupervisoryRaw; 
if object_id('tempdb..#SupervisoryResult') is not null drop table #SupervisoryResult; 
if object_id('tempdb..#SupervisoryResultPC') is not null drop table #SupervisoryResultPC; 


-- =================================================================================================================
-- Take copy of mapping table. Makes joins below easier to read. 
-- =================================================================================================================
select	CostCentreFrom, CostCentreTo
into	#BranchMapNonATSI
from	[CHSP].[Dim_BranchMapping]
where	[ATSIOnly] = 'F'; 

select	CostCentreFrom, CostCentreTo
into	#BranchMapATSI
from	[CHSP].[Dim_BranchMapping]
where	[ATSIOnly] = 'T'; 


-- =================================================================================================================
-- Derive branch surrogate key. 
-- Map certain branch codes to another branch.
-- For Aborigial clients, map to certain branch. 
-- =================================================================================================================
select	VisitID
		,ClientID
		,[ClientDOB]
		,[ServiceStartDate]
		,[ServiceEndDate]
		,a.BranchCodeOriginal
		,BranchCode
		,FunderCode
		,FunderID
		,FunderName				
		,br.Entity as BranchEntity
		,BillRecDescription
		,OrderDescription
		,coalesce(br.BranchKey, -1) as BranchKey		
		,coalesce(br.[LPAKey], -1) as [CHSPLPAKey]
		,[ServiceHoursRaw]		
		,[CurrentPostalCode]
		,[ATSIClient]
		,[EmployeeID]
		,[BillUnits]
		,[StagingServiceType]
into	#ResultPreBranch
from
(
		select	ac.VisitID
				,ac.ClientID
				,ac.[ClientDOB]
				,ac.[ServiceStartDate]
				,ac.[ServiceEndDate]
				,ac.BranchCode as BranchCodeOriginal
				--,coalesce(case when ac.[ATSIClient] = 'T' then brAtsi.CostCentreTo else brNonAtsi.CostCentreTo end, ac.BranchCode) as BranchCode
				,coalesce(case when ac.[ATSIClient] = 'T' then brAtsi.CostCentreTo else null end, ac.BranchCode) as BranchCode
				,ac.FunderCode
				,ac.FunderID
				,ac.FunderName								
				,ac.BillRecDescription
				,ac.OrderDescription				
				,[ServiceHoursRaw]		
				,ac.[CurrentPostalCode]
				,ac.[ATSIClient]		
				,ac.[EmployeeID]
				,ac.[BillUnits]
				,ac.ServiceType as StagingServiceType
		from	[CHSP].[Staging_Fact_ServiceHours] ac
				--left join #BranchMapNonATSI brNonAtsi on brNonAtsi.CostCentreFrom = replace(ac.BranchCode, 'D', '')
				left join #BranchMapATSI brAtsi on brAtsi.CostCentreFrom = replace(ac.BranchCode, 'D', '')	
		where	VerificationStatus = 'Verified'
		or		(VerificationStatus = 'Unverified' and VisitStatus = 'Active')
) a
		left join [CHSP].[Dim_Branch] br on br.CostCentre = replace(a.BranchCode, 'D', '')	
;


-- =================================================================================================================
-- Move Southern Highlands clients back from Lachlan. 
-- Move new clients from Lachlan to Southern Highlands. 
-- =================================================================================================================
select	sh.ClientID, lc.BranchCode as FromBranchCode, sh.BranchCode as ToBranchCode, LPAKey, sh.BranchKey as ToBranchKey
into	#MoveLachlantoSoutherHighlands
from 
(
		select	distinct ClientID, BranchCode, br.[BranchKey], br.LPAKey
		from	#ResultPreBranch pre
				inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = pre.BranchKey
		where	BranchCode = '6917'
		and		convert(date, [ServiceStartDate]) <= '2018-12-01'
		union
		select	distinct ClientID, BranchCode, br.[BranchKey], br.LPAKey
		from	[CHSP].[Fact_ServiceHours] ac 
				inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = ac.BranchKey
		where	BranchCode = '6917'
		and		convert(date, [ServiceStartDate]) <= '2018-12-01'
) sh
		inner join 
(
		select	distinct ClientID, BranchCode
		from	#ResultPreBranch pre
				inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = pre.BranchKey
		where	BranchCode = '6869'
		and		convert(date, [ServiceStartDate]) > '2018-12-01'
		union
		select	distinct ClientID, BranchCode
		from	[CHSP].[Fact_ServiceHours] ac 
				inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = ac.BranchKey
		where	BranchCode = '6869'
		and		convert(date, [ServiceStartDate]) > '2018-12-01'
) lc	on sh.ClientID = lc.ClientID

union 

select	sh.ClientID, lc.BranchCode as FromBranchCode, sh.BranchCode as ToBranchCode, LPAKey, sh.BranchKey as ToBranchKey
from 
(
		select	distinct ClientID, BranchCode, br.[BranchKey], br.LPAKey
		from	#ResultPreBranch pre
				inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = pre.BranchKey
		where	BranchCode = '6982'
		and		convert(date, [ServiceStartDate]) <= '2018-12-01'
		union
		select	distinct ClientID, BranchCode, br.[BranchKey], br.LPAKey
		from	[CHSP].[Fact_ServiceHours] ac 
				inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = ac.BranchKey
		where	BranchCode = '6982'
		and		convert(date, [ServiceStartDate]) <= '2018-12-01'
) sh
		inner join 
(
		select	distinct ClientID, BranchCode
		from	#ResultPreBranch pre
				inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = pre.BranchKey
		where	BranchCode = '6973'
		and		convert(date, [ServiceStartDate]) > '2018-12-01'
		union
		select	distinct ClientID, BranchCode
		from	[CHSP].[Fact_ServiceHours] ac 
				inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = ac.BranchKey
		where	BranchCode = '6973'
		and		convert(date, [ServiceStartDate]) > '2018-12-01'
) lc	on sh.ClientID = lc.ClientID

union

select	distinct ClientID, BranchCode as FromBranchCode, '6917' as ToBranchCode, br2.LPAKey, br2.BranchKey as ToBranchKey
from	#ResultPreBranch pre
		inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = pre.BranchKey
		inner join [CHSP].[Dim_Branch] br2 on br2.CostCentre = '6917'
where	BranchCode = '6869'
and		[CurrentPostalCode] in ('2594','2586' ,'2587')
and		convert(date, [ServiceStartDate]) > '2018-12-01'

union 

select	distinct ClientID, BranchCode as FromBranchCode, '6982' as ToBranchCode, br2.LPAKey, br2.BranchKey as ToBranchKey
from	#ResultPreBranch pre
		inner join [CHSP].[Dim_Branch] br on br.[BranchKey] = pre.BranchKey
		inner join [CHSP].[Dim_Branch] br2 on br2.CostCentre = '6982'
where	BranchCode = '6973'
and		[CurrentPostalCode] in ('2594','2586' ,'2587')
and		convert(date, [ServiceStartDate]) > '2018-12-01'
;


-- =================================================================================================================
-- Work out service types for RLM and non RLM branches. 
-- =================================================================================================================
select	VisitID
		,ClientID
		,[ClientDOB]
		,[ServiceStartDate]
		,[ServiceEndDate]		
		,BranchCodeOriginal
		,BranchCode
		,pre.FunderCode
		,FunderID
		,FunderName
		,BillRecDescription
		,OrderDescription
		,BranchKey
		,coalesce(fs.ServiceTypeKey, -1) as ServiceTypeKey
		,[CHSPLPAKey]
		,[ServiceHoursRaw]		
		,fst.ServiceceType as ServiceTypeOriginal
		,coalesce(fsm.[ServiceTypeTo], fst.ServiceceType) as ServiceType
		,pre.[CurrentPostalCode]
		,[ATSIClient]
		,[EmployeeID]
		,[BillUnits]
into	#ResultPreSupervisory
from	#ResultPreBranch pre
		left join Map.FunderCode_ServiceType fst on pre.FunderCode = fst.FunderCode
		left join [CHSP].[Dim_FundingServiceTypeMapping] fsm on fsm.[ServiceTypeFrom] = fst.ServiceceType
		left join [CHSP].[Dim_FundingService] fs on fs.[ServiceTypeDesc] =  coalesce(fsm.[ServiceTypeTo], fst.ServiceceType)
where	BranchEntity <> 'AURLM'
and		(
			fst.Funder in ('CHSP', 'CCSP', 'HACC', 'NRCP') 
			or pre.FunderCode like '%CHSP%' 
			or pre.FunderCode like '%HACC%'
			or ServiceType like '%CHSP%'
			or OrderDescription like '%CHSP%'
			or [StagingServiceType] like '%CHSP%'
			or pre.FunderCode in ('NRCP-CR-IH', 'NRCP-C-WS', 'BSOUT-HOMI', 'BSOUT-WHH', 'NRCP-CR-WS')
			--or pre.FunderCode in ('NRCP-CR-IH', 'NRCP-C-WS', 'BSOUT-HOMI', 'BSOUT-WHH')
			--or (pre.FunderCode = 'NRCP-CR-WS' AND [StagingServiceType] like 'IH%')
		)

union 

select	VisitID
		,ClientID
		,[ClientDOB]
		,[ServiceStartDate]
		,[ServiceEndDate]	
		,BranchCodeOriginal	
		,BranchCode
		,FunderCode
		,FunderID
		,FunderName
		,BillRecDescription
		,OrderDescription
		,BranchKey
		,coalesce(fs.ServiceTypeKey, -1) as ServiceTypeKey		
		,[CHSPLPAKey]
		,[ServiceHoursRaw]		
		,ServiceTypeOriginal	
		,coalesce(fsm.[ServiceTypeTo], a.ServiceTypeOriginal) as ServiceType
		,[CurrentPostalCode]
		,[ATSIClient]
		,[EmployeeID]
		,[BillUnits]
from	
(
		select	VisitID
				,ClientID
				,[ClientDOB]
				,[ServiceStartDate]
				,[ServiceEndDate]	
				,BranchCodeOriginal	
				,BranchCode
				,FunderCode
				,FunderID
				,FunderName
				,BillRecDescription
				,OrderDescription
				,BranchKey		
				,[CHSPLPAKey]
				,[ServiceHoursRaw]				
				,case 
					when BillRecDescription like '%SS:%' then 'Social Support Individual'
					when (BillRecDescription like '%Personal%' or BillRecDescription like '%PC%' or [OrderDescription] like '%Personal%' or BillRecDescription = 'Primary & Secondary Health Care') then 'Personal Care'
					when (BillRecDescription like '%Domestic%' or BillRecDescription like '%DA:%' or [OrderDescription] like '%Dome%') then 'Domestic Assistance'
					when (BillRecDescription like '%Resp%' or BillRecDescription like '%RC%' or BillRecDescription like '%DR%' or [OrderDescription] like '%Resp%') then 'Flexible Respite'
					when (BillRecDescription like '%Social%' or BillRecDescription like '%SS%' or BillRecDescription = 'Social Support Individual') then 'Social Support Individual'
					when (BillRecDescription = 'Allied Health') then 'Allied Health & Therapy Services'
					else BillRecDescription
					end	as ServiceTypeOriginal	
				,pre.[CurrentPostalCode]	
				,[ATSIClient]
				,[EmployeeID]
				,[BillUnits]
				,[StagingServiceType]
		from	#ResultPreBranch pre
		where	BranchEntity = 'AURLM'
) a
		left join [CHSP].[Dim_FundingServiceTypeMapping] fsm on fsm.[ServiceTypeFrom] = a.ServiceTypeOriginal
		left join [CHSP].[Dim_FundingService] fs on fs.[ServiceTypeDesc] = 	coalesce(fsm.[ServiceTypeTo], a.ServiceTypeOriginal)	

where	a.FunderCode like '%CHSP%' 
or		a.FunderCode like '%HACC%'
or		ServiceType like '%CHSP%'
or		OrderDescription like '%CHSP%'
or		[StagingServiceType] like '%CHSP%'
or		a.FunderCode in ('NRCP-CR-IH', 'NRCP-C-WS', 'BSOUT-HOMI', 'BSOUT-WHH', 'NRCP-CR-WS')
--or		a.FunderCode in ('NRCP-CR-IH', 'NRCP-C-WS', 'BSOUT-HOMI', 'BSOUT-WHH')
--or		(a.FunderCode = 'NRCP-CR-WS' AND [StagingServiceType] like 'IH%')
; 


-- =================================================================================================================
-- Derive supervisory hours based on service type. This needs to be added into service hours for CHSP as well.  
-- =================================================================================================================
select	VisitID
		,[ReqID]
		,ClientID
		,[ServiceStartDate]
		,[ServiceEndDate]
		,br.BranchKey as BranchKey	
		,BranchCodeOriginal
		,BranchCode
		,br.LPAKey as [CHSPLPAKey]
		,[ATSIClient]
		,[Requirement]
into	#SupervisoryRaw
from 
(
		select	VisitID, [ReqID], ClientID, coalesce(CompletedDate, DueDate) as [ServiceStartDate], coalesce(CompletedDate, [DueDate]) as [ServiceEndDate], ac.[BranchCode] as BranchCodeOriginal, [ATSIClient]
				--,coalesce(case when ac.[ATSIClient] = 'T' then brAtsi.CostCentreTo else brNonAtsi.CostCentreTo end, ac.BranchCode) as BranchCode
				,coalesce(case when ac.[ATSIClient] = 'T' then brAtsi.CostCentreTo else null end, ac.BranchCode) as BranchCode
				,[Requirement]
		from	[CHSP].[Staging_Fact_ServiceHoursSupervisory] ac
				--left join #BranchMapNonATSI brNonAtsi on brNonAtsi.CostCentreFrom = replace(ac.BranchCode, 'D', '')
				left join #BranchMapATSI brAtsi on brAtsi.CostCentreFrom = replace(ac.BranchCode, 'D', '')
		where	not (CompletedDate is null and DueDate < convert(date, getdate())) -- Don't take hours that are still not completed and have a due date before today. These are "expired"
) a
		inner join [CHSP].[Dim_Branch] br on br.CostCentre = replace(a.BranchCode, 'D', '')	
;

select	distinct [ReqID] as VisitID, sp.ClientID, sp.[ServiceStartDate], sp.[ServiceEndDate], [ReqID]
		,sp.BranchCodeOriginal, sp.BranchCode, sp.[CHSPLPAKey]
		,sp.BranchKey, pre.ServiceTypeKey, convert(decimal(38, 4), 2.00) as [ServiceHoursRaw], fs.ServiceTypeDesc as ServiceTypeOriginal, fs.ServiceTypeDesc as ServiceType
		,sp.[ATSIClient], convert(decimal(38, 4), 2.00) as SupervisoryHours
into	#SupervisoryResultPC
from	#SupervisoryRaw sp
		inner join #ResultPreSupervisory pre on sp.ClientID = pre.ClientID and sp.BranchKey = pre.BranchKey
		inner join [CHSP].[Dim_FundingService] fs on fs.ServiceTypeKey = pre.ServiceTypeKey
where	fs.ServiceTypeDesc = 'Personal Care'
;

select	distinct [ReqID] as VisitID, sp.ClientID, sp.[ServiceStartDate], sp.[ServiceEndDate]
		,sp.BranchCodeOriginal, sp.BranchCode, sp.[CHSPLPAKey]
		,sp.BranchKey, pre.ServiceTypeKey, convert(decimal(38, 4), 1.00) as [ServiceHoursRaw], fs.ServiceTypeDesc as ServiceTypeOriginal, fs.ServiceTypeDesc as ServiceType
		,sp.[ATSIClient], convert(decimal(38, 4), 1.00) as SupervisoryHours
into	#SupervisoryResult
from	#SupervisoryRaw sp
		inner join #ResultPreSupervisory pre on sp.ClientID = pre.ClientID and sp.BranchKey = pre.BranchKey
		inner join [CHSP].[Dim_FundingService] fs on fs.ServiceTypeKey = pre.ServiceTypeKey
where	fs.ServiceTypeDesc = 'Domestic Assistance'
and		not exists 
(
		select	null 
		from	#SupervisoryResultPC spc
		where	spc.[ReqID] = sp.[ReqID]
)

union all

select	VisitID, ClientID, [ServiceStartDate], [ServiceEndDate]
		,BranchCodeOriginal, BranchCode, [CHSPLPAKey]
		,BranchKey, ServiceTypeKey, [ServiceHoursRaw], ServiceTypeOriginal, ServiceType
		,[ATSIClient], SupervisoryHours
from	#SupervisoryResultPC
;


-- =================================================================================================================
-- Combine supervisory data with other visits. 
-- =================================================================================================================
select	VisitID, ClientID, [ClientDOB], [ServiceStartDate], [ServiceEndDate]
		,BranchCodeOriginal, BranchCode, FunderCode, FunderID, FunderName, BillRecDescription, OrderDescription
		,BranchKey, ServiceTypeKey, [CHSPLPAKey], [ServiceHoursRaw], ServiceTypeOriginal, ServiceType
		,[CurrentPostalCode], [ATSIClient], [EmployeeID], [BillUnits], convert(decimal(38, 4), null) as SupervisoryHours
into	#ResultPre
from	#ResultPreSupervisory psp
		
union all

select	VisitID, ClientID, convert(date, null) as [ClientDOB], [ServiceStartDate], [ServiceEndDate]
		,BranchCodeOriginal, BranchCode, convert(varchar, null) as FunderCode, convert(varchar, null) as FunderID, convert(varchar, null) as FunderName, convert(varchar, null) as BillRecDescription, convert(varchar, null) as OrderDescription
		,BranchKey, ServiceTypeKey, [CHSPLPAKey], [ServiceHoursRaw], ServiceTypeOriginal, ServiceType
		,convert(varchar, null) as [CurrentPostalCode], [ATSIClient], convert(varchar, null) as [EmployeeID], convert(decimal(38, 4), null) as [BillUnits], SupervisoryHours
from	#SupervisoryResult
;


-- =================================================================================================================
-- Absorb #MoveLachlantoSoutherHighlands. At the same time, override some of the Service Hours. 
-- Meals are calculated using units and Home Mods are using dollars. Transport is 1 per visit ID. 
-- Derive previous and next visit dates as foundation for Back to Back rounding logic. 
-- =================================================================================================================
select	*
		,case when (DATEDIFF(minute, [ServiceEndDate], NextVisitStart) <= 5) then convert(bit, 1) else convert(bit, 0) end as Back2BackStart
		,case when (DATEDIFF(minute, PreviousVisitEnd, [ServiceStartDate]) <= 5) then convert(bit, 1) else convert(bit, 0) end as Back2BackEnd
		,convert(bit, 0) as IsLastVisit
		,convert(decimal(38, 4), null) as ServiceHours
		,convert(decimal(38, 4), null) as ServiceHours3060
into	#Result
from	
(
		select	VisitID
				,pre.ClientID
				,[ClientDOB]
				,[ServiceStartDate]
				,[ServiceEndDate]	
				,lead([ServiceStartDate]) over (partition by pre.ClientID, [EmployeeID], convert(date, [ServiceStartDate]) order by [ServiceStartDate]) as NextVisitStart
				,lag([ServiceEndDate]) over (partition by pre.ClientID, [EmployeeID], convert(date, [ServiceStartDate]) order by [ServiceEndDate]) as PreviousVisitEnd
				,BranchCodeOriginal
				,coalesce(m.ToBranchCode, pre.BranchCode) as BranchCode
				,FunderCode
				,FunderID
				,FunderName
				,BillRecDescription
				,OrderDescription
				,coalesce(m.ToBranchKey, BranchKey) as BranchKey
				,ServiceTypeKey
				,coalesce(m.LPAKey, [CHSPLPAKey]) as [CHSPLPAKey]
				,[ServiceHoursRaw]				
				,ServiceTypeOriginal
				,ServiceType
				,pre.[CurrentPostalCode]
				,case when ([ATSIClient] = 'T') then convert(bit, 1) else convert(bit, 0) end as [ATSIClient]
				,[EmployeeID]
				,[BillUnits]	
				,SupervisoryHours	
		from	#ResultPre pre
				left join #MoveLachlantoSoutherHighlands m on m.ClientID = pre.ClientID and m.FromBranchCode = pre.BranchCode and convert(date, [ServiceStartDate]) > '2018-12-01'
) a 
;

-- Identify back to back visits that have a total visit duration less than 1 hour. 
-- Exclude cetain service types that do not measure in hours. 
select	[EmployeeID], ClientID, convert(date, [ServiceStartDate]) as VisitDate
into	#B2BLessThan1Hour
from	#Result
where	Back2BackStart = 1
or		Back2BackEnd = 1
and		ServiceType not in ('Home Mods ($)', 'Meals', 'Transport')
group by
		[EmployeeID], ClientID, convert(date, [ServiceStartDate])
having	sum([ServiceHoursRaw]) < 1
;

-- Identify last visit in the back to back sequence. 
-- This visit will be the one rounded up to 1 hour. 
select	[EmployeeID], ClientID, VisitID
into	#B2BLessThan1HourLastVisit
from 
(
		select	[EmployeeID], ClientID, VisitID, row_number() over (partition by [EmployeeID], ClientID, convert(date, [ServiceStartDate]) order by [ServiceStartDate] desc) as RowNumber
		from	#Result
		where	Back2BackStart = 1
		or		Back2BackEnd = 1
		and		ServiceType not in ('Home Mods ($)', 'Meals', 'Transport')
) a 
where	RowNumber = 1
;

-- Push last visit flag back into result
update	#Result
set		IsLastVisit = convert(bit, 1)
from	#Result r 
		inner join #B2BLessThan1HourLastVisit lv on lv.[EmployeeID] = r.[EmployeeID] and lv.ClientID = r.ClientID and lv.VisitID = r.VisitID
;

-- Push rounded hour back into result, specifically the last visit record in the sequence
update	#Result
set		[ServiceHoursRaw] = 1.00
from	#Result r 
		inner join #B2BLessThan1Hour lv on lv.[EmployeeID] = r.[EmployeeID] and lv.ClientID = r.ClientID and lv.VisitDate = convert(date, r.[ServiceStartDate])
where	IsLastVisit = 1
;

-- Treat Service Hours, rounding the other records up as needed
-- Only do this for non back to back visits. Back to back visits have been catered for above. 
update	#Result
set		ServiceHours = 
						case when (Back2BackStart = 0 and Back2BackEnd = 0) then 
							case
							when ServiceType in ('Home Mods ($)', 'Meals') then (case when [BillUnits] < 1.00 then 1 else [BillUnits] end)
							when ServiceType = 'Transport' then 1  
							else 
								case when [ServiceHoursRaw] < 1.00 then 1 else [ServiceHoursRaw] end
							end
						else [ServiceHoursRaw] end
		,ServiceHours3060 = 
						case when (Back2BackStart = 0 and Back2BackEnd = 0) then 
							case
							when ServiceType in ('Home Mods ($)', 'Meals') then (case when [BillUnits] < 1.00 then 1 else [BillUnits] end)
							when ServiceType = 'Transport' then 1  
							else 
								case 
								when [ServiceHoursRaw] <= 0.50 then 0.5
								when [ServiceHoursRaw] < 1.00 then 1.00
								else [ServiceHoursRaw] end
							end
						else [ServiceHoursRaw] end
; 


-- =================================================================================================================
-- Derive branch borrow visits. Keep the visit of the delivery branch and remove the visit from the owning branch. 
-- =================================================================================================================
select	distinct OwningVisitID
into	#BranchBorrow
from	#Result r 
		inner join [RDS].[BranchBorrowVisits] bb on r.VisitID = bb.DeliveryVisitId
;

delete	#Result
from	#Result r
		inner join #BranchBorrow bb on bb.OwningVisitID = r.VisitID
;


-- =================================================================================================================
-- Derive floor and ceiling dates. Will be used to delete record in current FY that did not come into staging
-- but exists in Fact. Happens when status in source is changed. 
-- =================================================================================================================
select	@FloorDate = min([ServiceStartDate]), @CeilingDate = max([ServiceStartDate])
from	#Result
;


-- =================================================================================================================
-- Whole bunch of LPA overrides for certain service types in certain branches. 
-- Need to find a way to implement this as a mapping table. It's weird because only the LPA is changed
-- instead of mapping to a branch inside the LPA. 
-- =================================================================================================================
update	#Result
set		CHSPLPAKey = (select [CHSPLPAKey] from CHSP.Dim_LPA where CHSPLPADesc = 'Northern Sydney')
from	#Result r
		inner join CHSP.Dim_Branch b on b.BranchKey = r.BranchKey
		inner join CHSP.Dim_FundingService fs on fs.ServiceTypeKey = r.ServiceTypeKey
where	fs.ServiceTypeDesc = 'Social Support Individual'
and		b.Department in ('RL NSW SE-EasternSyd', 'RL NSW MH-Cent Coast', 'RL NSW MH-Harbour N', 'RL NSW MH-Hunter Val', 'RL NSW MH-Lake Macq', 'RL NSW MH-Newcastle', 'RL NSW MH-Northn Syd', 'Mid North Coast')
; 

update	#Result
set		CHSPLPAKey = (select [CHSPLPAKey] from CHSP.Dim_LPA where CHSPLPADesc = 'Northern Sydney')
from	#Result r
		inner join CHSP.Dim_Branch b on b.BranchKey = r.BranchKey		
where	b.Department in ('RL NSW SE-Cronu-Suth', 'RL NSW MH-Harbour N')
; 

update	#Result
set		CHSPLPAKey = (select [CHSPLPAKey] from CHSP.Dim_LPA where CHSPLPADesc = 'Northern Sydney')
from	#Result r
		inner join CHSP.Dim_Branch b on b.BranchKey = r.BranchKey
		inner join CHSP.Dim_FundingService fs on fs.ServiceTypeKey = r.ServiceTypeKey
where	fs.ServiceTypeDesc = 'Domestic Assistance'
and		b.Department in ('Northern NSW', 'Eastern NSW', 'RL NSW SE-EasternSyd', 'RL NSW SE-St George', '')
;

update	#Result
set		CHSPLPAKey = (select [CHSPLPAKey] from CHSP.Dim_LPA where CHSPLPADesc = 'Northern Sydney')
from	#Result r
		inner join CHSP.Dim_Branch b on b.BranchKey = r.BranchKey
		inner join CHSP.Dim_FundingService fs on fs.ServiceTypeKey = r.ServiceTypeKey
where	fs.ServiceTypeDesc = 'Personal Care'
and		b.Department in ('RL NSW SE-St George', 'RL NSW MH-Cent Coast', 'RL NSW MH-Harbour N', 'RL NSW MH-Hunter Val', 'RL NSW MH-Lake Macq', 'RL NSW MH-Newcastle', 'RL NSW MH-Northn Syd', 'Mid North Coast')
;

update	#Result
set		CHSPLPAKey = (select [CHSPLPAKey] from CHSP.Dim_LPA where CHSPLPADesc = 'Northern Sydney')
from	#Result r
		inner join CHSP.Dim_Branch b on b.BranchKey = r.BranchKey
		inner join CHSP.Dim_FundingService fs on fs.ServiceTypeKey = r.ServiceTypeKey
where	fs.ServiceTypeDesc <> 'Domestic Assistance'
and		b.Department in ('Inner East VIC', 'RL VIC-Outer East', 'RL NSW MH-Lake Macq', 'RL NSW MH-Newcastle')
;

update	#Result
set		CHSPLPAKey = (select [CHSPLPAKey] from CHSP.Dim_LPA where CHSPLPADesc = 'Western Metro')
from	#Result r
		inner join CHSP.Dim_Branch b on b.BranchKey = r.BranchKey
		inner join CHSP.Dim_FundingService fs on fs.ServiceTypeKey = r.ServiceTypeKey
where	fs.ServiceTypeDesc = 'Domestic Assistance'
and		b.Department = 'RL VIC-Southern'
;

update	#Result
set		CHSPLPAKey = (select [CHSPLPAKey] from CHSP.Dim_LPA where CHSPLPADesc = 'Northern Metro')
from	#Result r
		inner join CHSP.Dim_Branch b on b.BranchKey = r.BranchKey
		inner join CHSP.Dim_FundingService fs on fs.ServiceTypeKey = r.ServiceTypeKey
where	fs.ServiceTypeDesc <> 'Domestic Assistance'
and		b.Department = 'RL VIC-Southern'
;

update	#Result
set		CHSPLPAKey = (select [CHSPLPAKey] from CHSP.Dim_LPA where CHSPLPADesc = 'Northern Metro')
from	#Result r
		inner join CHSP.Dim_Branch b on b.BranchKey = r.BranchKey
		inner join CHSP.Dim_FundingService fs on fs.ServiceTypeKey = r.ServiceTypeKey
where	fs.ServiceTypeDesc <> 'Domestic Assistance'
and		b.Department in ('Inner East VIC', 'RL VIC-Outer East')
;

update	#Result
set		CHSPLPAKey = (select [CHSPLPAKey] from CHSP.Dim_LPA where CHSPLPADesc = 'Eastern Metro')
from	#Result r
		inner join CHSP.Dim_Branch b on b.BranchKey = r.BranchKey
		inner join CHSP.Dim_FundingService fs on fs.ServiceTypeKey = r.ServiceTypeKey
where	fs.ServiceTypeDesc = 'Domestic Assistance'
and		b.Department in ('Inner East VIC', 'RL VIC-Outer East', 'RL VIC-Inner East')
;

update	#Result
set		CHSPLPAKey = (select [CHSPLPAKey] from CHSP.Dim_LPA where CHSPLPADesc = 'Mid North Coast')
		,BranchKey = (select BranchKey from CHSP.Dim_Branch where Department = 'HC NSW N-LowMN Coast')
		,BranchCode = '6879'
from	#Result r
		inner join CHSP.Dim_Branch b on b.BranchKey = r.BranchKey
		inner join CHSP.Dim_FundingService fs on fs.ServiceTypeKey = r.ServiceTypeKey
where	fs.ServiceTypeDesc <> 'Domestic Assistance'
and		b.Department = 'RL NSW N-LowMN Coast'
;

update	#Result
set		CHSPLPAKey = (select [CHSPLPAKey] from CHSP.Dim_LPA where CHSPLPADesc = 'Western Sydney')
from	#Result r
		inner join CHSP.Dim_Branch b on b.BranchKey = r.BranchKey
		inner join CHSP.Dim_FundingService fs on fs.ServiceTypeKey = r.ServiceTypeKey
where	fs.ServiceTypeDesc = 'Personal Care'
and		b.Department in ('RL NSW MW-Pros-Hills', 'RL NSW MW-SW Sydney')
;

update	#Result
set		CHSPLPAKey = (select [CHSPLPAKey] from CHSP.Dim_LPA where CHSPLPADesc = 'South West Sydney')
from	#Result r
		inner join CHSP.Dim_Branch b on b.BranchKey = r.BranchKey
		inner join CHSP.Dim_FundingService fs on fs.ServiceTypeKey = r.ServiceTypeKey
where	fs.ServiceTypeDesc = 'Social Support Individual'
and		b.Department in ('RL NSW MW-Pros-Hills', 'RL NSW MW-SW Sydney', 'RL NSW MW-Inn SW Syd')
;

update	#Result
set		CHSPLPAKey = (select [CHSPLPAKey] from CHSP.Dim_LPA where CHSPLPADesc = 'Hunter')
from	#Result r
		inner join CHSP.Dim_Branch b on b.BranchKey = r.BranchKey
		inner join CHSP.Dim_FundingService fs on fs.ServiceTypeKey = r.ServiceTypeKey
where	fs.ServiceTypeDesc = 'Domestic Assistance'
and		b.Department in ('RL NSW MH-Lake Macq', 'RL NSW MH-Newcastle')
;

update	#Result
set		CHSPLPAKey = (select [CHSPLPAKey] from CHSP.Dim_LPA where CHSPLPADesc = 'Orana Far West')
from	#Result r
		inner join CHSP.Dim_Branch b on b.BranchKey = r.BranchKey		
where	FunderName like '%Orana%'
and		(b.Department like '%Calare%' or b.Department like '%Lachlan%')
;


-- =================================================================================================================
-- Merge into target
-- =================================================================================================================
merge	[CHSP].[Fact_ServiceHours] t
using	
		(
			select	r.*, coalesce(brNonAtsi.CostCentreTo, r.BranchCode) as BranchCodeFinal, coalesce(br.BranchKey, r.BranchKey) as BranchKeyFinal, coalesce(br.LPAKey, r.[CHSPLPAKey]) as LPAKeyFinal
			from	#Result r
					left join #BranchMapNonATSI brNonAtsi on brNonAtsi.CostCentreFrom = replace(r.BranchCode, 'D', '')
					left join [CHSP].[Dim_Branch] br on br.CostCentre = replace(brNonAtsi.CostCentreTo, 'D', '')	
		) r		
		on (t.[VisitID] = r.[VisitID] and t.[ClientID] = r.[ClientID])

when	matched then update set
		t.[ClientDOB] = r.[ClientDOB]
		,t.[ServiceStartDate] = r.[ServiceStartDate]
		,t.[ServiceEndDate] = r.[ServiceEndDate]		
		,t.[FunderID] = r.[FunderID]
		,t.[FunderCode] = r.[FunderCode]
		,t.[FunderName] = r.[FunderName]
		,t.[BranchCodeOriginal] = r.[BranchCodeOriginal]
		,t.[BranchCode] = r.BranchCodeFinal
		,t.[BillRecDescription] = r.[BillRecDescription]	
		,t.[OrderDescription] = r.[OrderDescription]	
		,t.[ServiceTypeOriginal] = r.[ServiceTypeOriginal]	
		,t.[ServiceType] = r.[ServiceType]				
		,t.[BranchKey] = r.BranchKeyFinal
		,t.[ServiceTypeKey] = r.[ServiceTypeKey]
		,t.[CHSPLPAKey] = r.LPAKeyFinal		
		,t.[ServiceHoursRaw] = r.[ServiceHoursRaw]		
		,t.[ServiceHours] = r.[ServiceHours]		
		,t.[CurrentPostalCode] = r.[CurrentPostalCode]		
		,t.[ATSIClient] = r.[ATSIClient]
		,t.[EmployeeID] = r.[EmployeeID]
		,t.[IsLastVisit] = r.[IsLastVisit]
		,t.[Back2BackStart] = r.[Back2BackStart]
		,t.[Back2BackEnd] = r.[Back2BackEnd]
		,t.[NextVisitStart] = r.[NextVisitStart]
		,t.[PreviousVisitEnd] = r.[PreviousVisitEnd]
		,t.[BillUnits] = r.[BillUnits]
		,t.SupervisoryHours = r.SupervisoryHours
		,t.ServiceHours3060 = r.ServiceHours3060
		
when	not matched then insert
		(
			[VisitID]
			,[ClientID]
			,[ClientDOB]
			,[ServiceStartDate]
			,[ServiceEndDate]
			,[FunderID]
			,[FunderCode]
			,[FunderName]
			,[BranchCodeOriginal]
			,[BranchCode]
			,[BillRecDescription]
			,[OrderDescription]
			,[ServiceTypeOriginal]
			,[ServiceType]
			,[BranchKey]
			,[ServiceTypeKey]
			,[CHSPLPAKey]
			,[ServiceHoursRaw]
			,[ServiceHours]
			,[CurrentPostalCode]
			,[ATSIClient]
			,[EmployeeID]
			,[IsLastVisit]
			,[Back2BackStart]
			,[Back2BackEnd]
			,[NextVisitStart]
			,[PreviousVisitEnd]
			,[BillUnits]
			,SupervisoryHours
			,ServiceHours3060
		)
		values
		(
			r.[VisitID]
			,r.[ClientID]
			,r.[ClientDOB]
			,r.[ServiceStartDate]
			,r.[ServiceEndDate]
			,r.[FunderID]
			,r.[FunderCode]
			,r.[FunderName]
			,r.[BranchCodeOriginal]
			,r.BranchCodeFinal
			,r.[BillRecDescription]
			,r.[OrderDescription]
			,r.[ServiceTypeOriginal]
			,r.[ServiceType]
			,r.BranchKeyFinal
			,r.[ServiceTypeKey]
			,r.LPAKeyFinal
			,r.[ServiceHoursRaw]
			,r.[ServiceHours]	
			,r.[CurrentPostalCode]
			,r.[ATSIClient]
			,r.[EmployeeID]
			,r.[IsLastVisit]
			,r.[Back2BackStart]
			,r.[Back2BackEnd]
			,r.[NextVisitStart]
			,r.[PreviousVisitEnd]
			,r.[BillUnits]
			,r.SupervisoryHours
			,r.ServiceHours3060
		)

when	not matched by source and convert(date, t.[ServiceStartDate]) between @FloorDate and @CeilingDate then
		delete
;

