﻿

CREATE procedure [CHSP].[usp_ServiceControl_Output] 
as
-- exec [CHSP].[usp_ServiceControl_Output]
-- =================================================================================================================
--
-- Author:			M.Rama
--
-- Description:		Outputs combined data
--
-- Purpose:			For dashboards. Beats views anyday as views are not giving parallel plans with joined with DimDate
--					
--
-- Run Frequency:	TBD
--
-- =================
-- Revision History:
-- =================								 			
--
-- Version		Date		Who		Description		
-- ----------------------------------------------------------------------------------------------------------------
--  1.0			03/04/2019	M.Rama		Initial
-- =================================================================================================================


-- =================================================================================================================
-- Turn off messages SQL sends back to the client after each TSQL Statement
-- =================================================================================================================
set nocount on;


-- =================================================================================================================
-- Prepare working tables
-- =================================================================================================================
if object_id('tempdb..#Result') is not null drop table #Result; 


-- =================================================================================================================
-- Localize parameter
-- =================================================================================================================
declare	@MinFullDateAlternateKey date; 
declare	@MaxFullDateAlternateKey date; 
declare	@MaxFullDateAlternateKey2 date; 


-- =================================================================================================================
-- Determine floor and ceiling of current financial year. This fixed the parallel query plan problem.
-- =================================================================================================================
select	@MinFullDateAlternateKey = convert(date, min([FullDateAlternateKey]))
from	[CHSP].[Dim_Date] 
where	convert(int, FinancialYear) = (select FinancialYear from [CHSP].[Dim_Date] where FullDateAlternateKey = convert(date, getdate())); 

select	@MaxFullDateAlternateKey = convert(date, max([FullDateAlternateKey]))
from	[CHSP].[Dim_Date] 
where	convert(int, FinancialYear) = (select FinancialYear from [CHSP].[Dim_Date] where FullDateAlternateKey = convert(date, getdate())); 

select	@MaxFullDateAlternateKey2 = dateadd(day, -1 * (day(_Input)), _Input)
from	
(
		select	dateadd(month, 3, convert(date, getdate())) as _Input
) a

set		@MaxFullDateAlternateKey = case when (@MaxFullDateAlternateKey2 <= @MaxFullDateAlternateKey) then @MaxFullDateAlternateKey2 else @MaxFullDateAlternateKey end;   


-- =================================================================================================================
-- Output big table
-- Orana Far West branch stopped recording clients at the end of 2018. Need to keep sending last count so that
-- Cummulative calculations in dashboard will work. 
-- =================================================================================================================
select	br.CostCentre as [Branch Code]
		,br.[Department] as [Branch Name]
		,replace(br.Entity, 'AURLM', 'AUHCS') as [Branch Entity]
		,br.State
		--,case 
		--	when (ac.[FunderName] like '%Orana%' and br.Department like '%Calare%') then 'Orana Far West'
		--	when (ac.[FunderName] like '%Orana%' and br.Department like '%LachLan%') then 'Orana Far West'
		--	else lpa.CHSPLPADesc
		--	end as LPA	
		--,case when ((br.Department like '%Calare%' or br.Department like '%Lachlan%') and FunderName like '%Orana%') then 'Orana Far West' else lpa.CHSPLPADesc end as LPA
		,lpa.CHSPLPADesc as LPA			
		,convert(date, convert(varchar, convert(int, convert(varchar, ServiceStartDate, 112)) / 100) + '01') as [Service Month]
		,count(distinct [ClientID]) as [Serviced Clients]
into	#Result
from	[CHSP].[Fact_ServiceHours] ac
		inner join [CHSP].[Dim_Branch] br on br.BranchKey = ac.BranchKey
		inner join [CHSP].[Dim_LPA] lpa on lpa.CHSPLPAKey = ac.CHSPLPAKey
		inner join [CHSP].[Dim_FundingService] fs on fs.ServiceTypeKey = ac.ServiceTypeKey		
where	ServiceStartDate between @MinFullDateAlternateKey and @MaxFullDateAlternateKey	
and		fs.[ToDashboard] = 1
group by
		br.CostCentre
		,br.[Department]
		,br.Entity
		,br.State
		--,case 
		--	when (ac.[FunderName] like '%Orana%' and br.Department like '%Calare%') then 'Orana Far West'
		--	when (ac.[FunderName] like '%Orana%' and br.Department like '%LachLan%') then 'Orana Far West'
		--	else lpa.CHSPLPADesc
		--	end		
		--,case when ((br.Department like '%Calare%' or br.Department like '%Lachlan%') and FunderName like '%Orana%') then 'Orana Far West' else lpa.CHSPLPADesc end
		,lpa.CHSPLPADesc
		,convert(date, convert(varchar, convert(int, convert(varchar, ServiceStartDate, 112)) / 100) + '01')
;

with	c_max
as
(
		select	[Branch Code], [Branch Name], [Branch Entity], State, LPA, [Serviced Clients], [Service Month]
				, row_number() over (partition by [Branch Code], [Branch Name], [Branch Entity], State, LPA order by [Service Month] desc) as [Max Service Month]
		from	#Result r						
		where	[Branch Name] = 'HC NSW W-Orana'
		and		[Branch Entity] = 'AUHCS'
		and		LPA = 'Orana Far West'		
)
select	[Branch Code], [Branch Name], [Branch Entity], State, LPA, [Service Month], [Serviced Clients]
from	#Result
union all
select	distinct [Branch Code], [Branch Name], [Branch Entity], State, LPA, dd.FirstDayOfMonth as [Service Month], [Serviced Clients]
from	c_max m
		inner join CHSP.Dim_Date dd on 1 = 1 and dd.FirstDayOfMonth > m.[Service Month] and dd.FirstDayOfMonth <= '2019-06-30' --@MaxFullDateAlternateKey
and		[Max Service Month] = 1
;
