﻿


CREATE procedure [CHSP].[usp_Fact_Funding_Load] 
(
	@FinancialYear int = NULL
) 
as
-- exec [CHSP].[usp_Fact_Funding_Load] @FinancialYear = 2018
-- =================================================================================================================
--
-- Author:			M.Rama
--
-- Description:		Merges data from source file into fact table
--
-- Purpose:			Base of daily spread of funding
--					
--
-- Run Frequency:	TBD
--
-- =================
-- Revision History:
-- =================								 			
--
-- Version		Date		Who		Description		
-- ----------------------------------------------------------------------------------------------------------------
--  1.0			03/04/2019	M.Rama		Initial
-- =================================================================================================================


-- =================================================================================================================
-- Turn off messages SQL sends back to the client after each TSQL Statement
-- =================================================================================================================
set nocount on;


-- =================================================================================================================
-- Localize parameter
-- =================================================================================================================
declare @IFinancialYear int = @FinancialYear; 
--declare @IFinancialYear int = 2018; 
declare @ModifiedDate datetime = getdate(); 


-- =================================================================================================================
-- Prepare working tables
-- =================================================================================================================
if object_id('tempdb..#Result') is not null drop table #Result; 


-- =================================================================================================================
-- Pickup [CHSPAgreement] from branch and resolve Surrogate Keys
-- =================================================================================================================
select	st.[FinancialYear]
		,st.[AgreementLPA]
		,st.[UnitType]
		,st.[Units] as [FullYear_Funded_Units]
		,replace(st.[BranchCode], 'D', '') as [BranchCode]
		,st.[ServiceType]
		,br.Entity as [CHSPAgreement]
		,coalesce(br.BranchKey, -1) as [BranchKey]
		,coalesce(lpa.[CHSPLPAKey], -1) as [CHSPLPAKey]
		,coalesce(fs.[ServiceTypeKey], -1) as [ServiceTypeKey]
		,coalesce(a.[CHSPAgreementTypeKey], -1) as [CHSPAgreementTypeKey]
into	#Result
from	[CHSP].[Staging_Fact_Funding] st		
		left join [CHSP].[Dim_Branch] br on br.CostCentre = replace(st.[BranchCode], 'D', '')
		left join [CHSP].[Dim_LPA] lpa on lpa.CHSPLPA = st.AgreementLPA
		left join [CHSP].[Dim_FundingServiceTypeMapping] fsm on fsm.[ServiceTypeFrom] = st.ServiceType
		left join [CHSP].[Dim_FundingService] fs on fs.ServiceTypeDesc = coalesce(fsm.[ServiceTypeTo], st.ServiceType)
		left join [CHSP].[Dim_Agreement] a on a.CHSPAgreementType = br.Entity;

-- =================================================================================================================
-- Merge into target
-- =================================================================================================================
merge	[CHSP].[Fact_Funding] t
using	#Result r 
		on (t.[FinancialYear] = r.[FinancialYear] and t.[AgreementLPA] = r.[AgreementLPA] and t.BranchCode = r.BranchCode and t.ServiceType = r.ServiceType)

when	matched then update set
		t.[FullYear_Funded_Units] = r.[FullYear_Funded_Units]
		,t.[UnitType] = r.[UnitType]
		,t.[CHSPAgreement] = r.[CHSPAgreement]
		,t.[BranchKey] = r.[BranchKey]
		,t.[CHSPLPAKey] = r.[CHSPLPAKey]
		,t.[ServiceTypeKey] = r.[ServiceTypeKey]
		,t.[CHSPAgreementTypeKey] = r.[CHSPAgreementTypeKey]
		,t.[ModifiedDate] = @ModifiedDate

when	not matched then insert
		(
			[FinancialYear]
			,[AgreementLPA]			
			,[BranchCode]			
			,[ServiceType]	
			,[UnitType]		
			,[FullYear_Funded_Units]		
			,[CHSPAgreement]
			,[BranchKey]
			,[CHSPLPAKey]
			,[ServiceTypeKey]
			,[CHSPAgreementTypeKey]
			,[ModifiedDate]
		)
		values
		(
			r.[FinancialYear]
			,r.[AgreementLPA]			
			,r.[BranchCode]			
			,r.[ServiceType]		
			,r.[UnitType]	
			,r.[FullYear_Funded_Units]			
			,r.[CHSPAgreement]
			,r.[BranchKey]
			,r.[CHSPLPAKey]
			,r.[ServiceTypeKey]
			,r.[CHSPAgreementTypeKey]
			,@ModifiedDate
		)
;


-- =================================================================================================================
-- Clean up orphans
-- =================================================================================================================
delete
from	[CHSP].[Fact_Funding]
where	[FinancialYear] = @IFinancialYear
and		[ModifiedDate] <> @ModifiedDate; 
