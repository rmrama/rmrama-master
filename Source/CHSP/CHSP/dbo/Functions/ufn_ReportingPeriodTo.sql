﻿create FUNCTION [dbo].[ufn_ReportingPeriodTo]()
RETURNS Date
AS
BEGIN

--SET DATEFIRST 7

Declare @Today datetime
set @Today = getdate()

Declare @ReturnValue DATE
select @ReturnValue =(
		CASE 
				WHEN 
					day(@Today) =1
					then dateadd(d,day(@today)*-1,@Today)
					
				WHEN 
						day(@Today) =2
						AND Datepart(DW, @Today)  IN (
							1
							,2
							
						)
						
					then dateadd(d,day(@today)*-1,@Today)
					
				WHEN 
						day(@Today) =2
						AND Datepart(DW, @Today)  not IN (
							1
							,2
							
						)
					THEN @Today
					
					WHEN 
						day(@Today) =3
						AND Datepart(DW, @Today)  IN (
							2
							
						)
						
					THEN dateadd(d,day(@today)*-1,@Today)
					WHEN 
						day(@Today) =3
						AND Datepart(DW, @Today) not  IN (
							2
							
						)
						THEN @Today
					
					WHEN day(@Today) >3 
					THEN @Today
					

				ELSE (0)
				END)


	RETURN @ReturnValue
		END