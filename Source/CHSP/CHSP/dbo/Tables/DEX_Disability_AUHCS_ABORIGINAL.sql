﻿CREATE TABLE [dbo].[DEX_Disability_AUHCS_ABORIGINAL] (
    [ID]             INT          IDENTITY (1, 1) NOT NULL,
    [ClientID]       VARCHAR (14) NULL,
    [DisabilityCode] VARCHAR (20) NULL,
    [Entity]         VARCHAR (20) NULL
);

