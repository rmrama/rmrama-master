﻿CREATE TABLE [dbo].[DEX_Cases_AUHCS_ABORIGINAL] (
    [ID]                               INT           IDENTITY (1, 1) NOT NULL,
    [ClientID]                         VARCHAR (14)  NOT NULL,
    [CaseID]                           VARCHAR (14)  NULL,
    [OutletactivityId]                 VARCHAR (100) NULL,
    [TotalNumberOfUnidentifiedClients] INT           NOT NULL,
    [Entity]                           VARCHAR (20)  NULL
);

