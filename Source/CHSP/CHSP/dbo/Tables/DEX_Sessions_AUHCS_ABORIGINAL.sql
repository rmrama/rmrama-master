﻿CREATE TABLE [dbo].[DEX_Sessions_AUHCS_ABORIGINAL] (
    [ID]                                INT             IDENTITY (1, 1) NOT NULL,
    [ClientID]                          VARCHAR (14)    NOT NULL,
    [SessionID]                         VARCHAR (14)    NOT NULL,
    [CaseID]                            VARCHAR (14)    NULL,
    [SessionDate]                       DATETIME        NULL,
    [ServiceTypeId]                     INT             NOT NULL,
    [TotalNumberOfUnidentifiedClients4] INT             NOT NULL,
    [FeesCharged]                       NUMERIC (38, 6) NULL,
    [InterpreterPresent]                VARCHAR (5)     NOT NULL,
    [ParticipationCode]                 VARCHAR (6)     NOT NULL,
    [TimeMinutes]                       DECIMAL (38, 4) NULL,
    [Entity]                            VARCHAR (20)    NULL
);

